// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file prepare_src_files_for_current_OS.h
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.5.0
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the LCG Core Cpp Library.
//
//                          License Agreement
//                     For the LCG Core Cpp Library
//
//    LCG Core Cpp Library is a lightweight open source library used to wrap
//    pthreads, Berkeley sockets, and other C libraries and functionalities,
//    into C++ objectes, over Linux, Mac OS and Windows.
//
//    LCG Core Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    LCG Core Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include <string>                           // std::string

/**
 * @brief start Starts the new thread defined by the run method.
 * This method should be called after a new thread has been derived from
 * this class and the run method  and exit methods have been implemented.
 * The new thread will start at the run method, and the contents of the
 * run method should be given by the implementation on the deirved class.
 *
 * The method of exitting or "stopping" the new thread should be given
 * by implementing the exit method on the derived class (by means of changing
 * a centinel variable inside the thread, for example).
 * @return 0 on success. On failure it rises an exception of type std::domain_error
 * @see exit, run.
 */

/**
 * @brief prepare_src_files_for_current_OS prepares the source files for installation on current platform.
 * The argc and argv parameters should be the same passed by the "int main(int argc, char** argv)"
 * main function. This arguments represent the inputFileToParse and outputFilePath
 * given to this program by the command line.
 * The order is: thisProgramName inputFileToParse outputFileToParse
 *
 * If more than two arguments are given to this function (strings on the command
 * line), the function will return immediately with a value of -1;
 *
 * @param argc This should be the same argc that "int main(int argc, char** argv)"
 * gives on the main program.
 * @param argv This should be the same argv that "int main(int argc, char** argv)"
 * gives on the main program.
 * @return On success 0. On failure, a non zero number or an std::invalid_arugment
 * exception will be thrown.
 */
int prepare_src_files_for_current_OS(int argc, char **argv);
