// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file prepare_src_files_for_current_OS.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.5.0
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the LCG Core Cpp Library.
//
//                          License Agreement
//                     For the LCG Core Cpp Library
//
//    LCG Core Cpp Library is a lightweight open source library used to wrap
//    pthreads, Berkeley sockets, and other C libraries and functionalities,
//    into C++ objectes, over Linux, Mac OS and Windows.
//
//    LCG Core Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    LCG Core Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include "lcg/core/OSSpecificFileParser.h"  // lcg::core::OSSpecificFileParser
#include <string>                           // std::string

#ifdef __WINDOWS
#define CURRENT_PLATFORM "__WINDOWS"
#endif // __WINDOWS
#ifdef __LINUX
#define CURRENT_PLATFORM "__LINUX"
#endif // __LINUX

int prepare_src_files_for_current_OS(int argc, char **argv){
    if(argc != 3) {     // If no inputFileToParse and outputFilePath is given
        return -1;
    }
    std::string inputFileToParse = argv[1];
    std::string outputFilePath = argv[2];


    lcg::core::OSSpecificFileParser parser;
    parser.addPlatformMacro("__WINDOWS");
    parser.addPlatformMacro("__LINUX");

    parser.setInputFilePath(inputFileToParse);
    parser.parse(CURRENT_PLATFORM);
    parser.writeToPath(outputFilePath);
}
