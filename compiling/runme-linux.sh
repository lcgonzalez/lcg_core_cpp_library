#!/bin/bash

LICENSE="Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.

  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.

    This file is part of the LCG Core Cpp Library.

                          License Agreement
                     For the LCG Core Cpp Library

    LCG Core Cpp Library is a lightweight open source library used to wrap
    pthreads, Berkeley sockets, and other C libraries and functionalities,
    into C++ objectes, over Linux, Mac OS and Windows.

    LCG Core Cpp Library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    LCG Core Cpp Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, see <http://www.gnu.org/licenses/>,
    or write to the Free Software Foundation, Inc., 51 Franklin Street,
    Fifth Floor, Boston, MA  02110-1301  USA.

    "
    
## Manual variable declarations ##
# If needed, configure the building and installation process from here

includeInstallDir="/usr/local/include"
binInstallDir="/usr/local/lib"

binTmpOutputDir="build_release"
libraryName="LCG_Core_Cpp"
libraryHumanName="LCG Core Cpp"
libraryVersion="0.7.0"
namespaces="lcg/core"

## Non configurable declarations ##
# Don't modify these

fullLibraryName=lib$libraryName
    
package="runme-linux"
testing=0 		# Not testing
installFail=0	# 0 Success, 1 otherwise
uninstallFail=0	# 0 Success, 1 otherwise
docFail=0		# 0 Success, 1 otherwise

# Get script path and cd to that directory
scriptDir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
cd $scriptDir
# cd to the project root to start the building process
cd ../
projectRootDir=$(pwd)
tmpInstallDir="$projectRootDir/compiling/install/prepare_src_files_for_current_OS"

#################################
######## Functions ##############
#################################

# Check if a given library is installed in the system
# Usage: check_library_is_installed <library_name_to_check> <variable_to_store_the_result>
# If the library_name_to_check is installed, variable_to_store_the_result=1,
# otherwise variable_to_store_the_result=0
#
# Example: check_library_is_installed "LCG_Core_Cpp" libInstalled
function check_library_is_installed() {
	local __libraryNameToCheck=$1
	local __resultVar=$2
	
	echo "Checking if library $__libraryNameToCheck is installed."
	ldconfig -p | grep lib$__libraryNameToCheck > /dev/null
	if [ $? -ne 0 ]; then	# Not installed
		__libInstalled=0
		echo "Library $__libraryNameToCheck not installed!"
	else					# Already installed
		__libInstalled=1
		echo "Library $__libraryNameToCheck installed!"
	fi
	
	eval $__resultVar="'$__libInstalled'"
}

#################################
######## Main script ############
#################################

while test $# -gt 0; do
	case "$1" in
		-v|--version)
			echo "$libraryHumanName library version $libraryVersion"
			exit 0
			;;
		-h|--help)
			echo "$package - build $libraryHumanName library"
			echo " "
			echo "$package [options]"
			echo " "
			echo "options:"
			echo "tests			Run unit tests on the library"
			echo "install			Install the library"
			echo "uninstall		Uninstall the library"
			echo "doc			Build Doxygen documentation"
			echo "clean			Remove all building related files"
			echo "-h, --help		Show brief help"
			echo "-v, --version		Show library version"
			echo "--license		Show the library license"
			exit 0
			;;
		--license)
			echo "$LICENSE"
			exit 0
			;;
		doc)
			echo "The project's public API documentation is being built."
			doxygen doxygen_documentation.config
			if [ $? -ne 0 ]; then
				echo "Error while building the project documentation on $projectRootDir/doxygen_doc"
				docFail=1
				break
			fi
			echo "Documentation successfully built on $projectRootDir/doxygen_doc"
			exit 0
			;;		
		tests)
			testing=1
			break;
			;;
		clean)
			echo "Cleaning the building directories."
			rm -rf $binTmpOutputDir unit_tests/$binTmpOutputDir unit_tests/gtest $tmpInstallDir/build
			echo "Cleaning complete!"
			exit 0
			;;
		uninstall)
			# Before anything else, check if the library is not installed, if it is not, abort
			check_library_is_installed $libraryName isLibInstalled
			if [ $isLibInstalled -eq 0 ]; then
				echo "$libraryHumanName library not installed, aborting uninstallation process."
				exit 1
			fi
		
		
			echo "Uninstalling $fullLibraryName library."
			echo " "
			
			binFilesToDelete=$(ls $binInstallDir/$fullLibraryName.so*)
			includeFilesToDelete=$(ls $includeInstallDir/$namespaces)
			echo "Listing shared object files to delete:"
			echo $binFilesToDelete
			echo ""
			echo "Listing include files to delete in $includeInstallDir/$namespaces folder:"
			echo $includeFilesToDelete
			echo ""
			echo "Are you sure you want to delete those files?"
			read userInput
			if [ "$userInput" != "y" ]; then
				if [ "$userInput" != "Y" ]; then
				echo "Aborting uninstallation. No file was deleted."
				exit 1
				fi
			fi
			
			echo "Deleting shared object files: "
			sudo rm -rf $binInstallDir/$fullLibraryName.so*
			if [ $? -ne 0 ]; then
				echo "Error while removing the shared object files from system."
				uninstallFail=1
				break
			fi
			
			echo "Deleting include directory: $includeInstallDir/$namespaces"
			sudo rm -rf $includeInstallDir/$namespaces
			if [ $? -ne 0 ]; then
				echo "Error while removing the include files from system."
				uninstallFail=1
				break
			fi
			
			echo "Unregistering library from system using ldconfig"
			sudo ldconfig
			echo "Uninstallation complete!"
			exit 0
			;;
		install)
			# Before anything else, check if the library is already installed, if it is, abort
			check_library_is_installed $libraryName isLibInstalled
			if [ $isLibInstalled -eq 1 ]; then
				echo "$libraryHumanName library already installed, aborting installation process."
				exit 1
			fi
		
		
			# First compile the parser utility
			echo "Installing $fullLibraryName library."
			echo " "
			echo "Compiling parsing utility"
			cd $tmpInstallDir
			mkdir build
			cd build
			cmake ../ && make
			
			# Create the output dirs and get the input filenames
			installExeName="prepare_src_files_for_current_OS"
			installExePath=$tmpInstallDir/build/$installExeName
			cd $projectRootDir		# cd to project root
			includePath="include/$namespaces"
			includeOutputParsedPath="./compiling/install/src_files_ready_to_install"
			outputPath="$includeOutputParsedPath/$includePath"
			mkdir -p $outputPath
			# Parse all the files in the include dir
			echo "Parsing files:"
			FILES="$includePath/*"
			for f in $FILES; do
				filename=${f##*/}
				echo "Processing $filename file..."
				$installExePath $f $outputPath/$filename
				if [ $? -ne 0 ]; then
					installFail=1
					break
				fi
			done
			
			if [ $installFail -ne 0 ]; then
				echo "Error while parsing source files."
				break
			fi
			
			echo "Parsing complete!"
			echo " "
			
			# Copy files to system directories
			# Include files
			echo "Installing include files on $includeInstallDir/$namespaces"
			sudo cp -r $includeOutputParsedPath/include/lcg $includeInstallDir
			if [ $? -ne 0 ]; then
				echo "Error while copying include files to system."
				installFail=1
				break
			fi
			sudo chmod -R 755 $includeInstallDir/lcg		# Fix permissions
			
			# Binary file
			binFiles=$(ls $binTmpOutputDir/$fullLibraryName.so.*)
			binFilename=${binFiles##*/}
			echo "Installing library object file $binFilename on $binInstallDir"
			sudo cp -r $binTmpOutputDir/$binFilename $binInstallDir
			if [ $? -ne 0 ]; then
				echo "Error while copying shared object files to system."
				installFail=1
				break
			fi
			
			echo "Creating symbolic link from $binInstallDir/$fullLibraryName.so -> $binInstallDir/$binFilename"
			sudo ln -sf $binInstallDir/$binFilename $binInstallDir/$fullLibraryName.so
			if [ $? -ne 0 ]; then
				echo "Error while creating symbolic link to shared objects on system."
				installFail=1
				break
			fi
			
			echo "Registering library on system using ldconfig"
			sudo ldconfig	# Register the new library on the system
			echo "Installation complete!"
			exit 0
			;;
		*)
			echo "Unknown option. Please use the -h option for more information."
			exit 0
			;;
	esac
done

if [ $installFail -ne 0 ]; then
	echo " "
	echo "The installation process failed!"
	echo "Please check that the library had already been built before attempting to install it."
	exit -1
elif [ $uninstallFail -ne 0 ]; then
	echo " "
	echo "The uninstallation process failed!"
	echo "Please check that the the library is installed in order to uninstall it."
	echo "If it is installed, and the correct permissions are given to this script,"
	echo "please refer to a manual unistallation."
	exit -1
elif [ $docFail -ne 0 ]; then
	echo " "
	echo "The generation of Doxygen documentation failed!"
	echo "Please check that doxygen is installed on your system in order to generate"
	echo "the documentation of the project's public API."
fi 	

## Build project, if not already built
mkdir $binTmpOutputDir
cd $binTmpOutputDir
if [ $testing -eq 0 ]; then	# Just build, without unit testing
	cmake ../ -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTING=False && make
else						# Build and run unit tests
	cmake ../ -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTING=True && make && make test
fi

