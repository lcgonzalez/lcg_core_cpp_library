// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file OSSpecificFileParserTests.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.6.2
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the LCG Core Cpp Library.
//
//                          License Agreement
//                     For the LCG Core Cpp Library
//
//    LCG Core Cpp Library is a lightweight open source library used to wrap
//    pthreads, Berkeley sockets, and other C libraries and functionalities,
//    into C++ objectes, over Linux, Mac OS and Windows.
//
//    LCG Core Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    LCG Core Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/


#include "lcg/core/OSSpecificFileParser.h"
#include "lcg/core/System.h"

#include <gtest/gtest.h>

std::string exeDir = lcg::core::System::getExeDir();    // Make paths absolute to avoid relative paths problems

class OSSpecificFileParser_compareTextFiles_test : public ::testing::Test{
public:
    std::string nonexistentPath;
    std::string testInputFilePath_A;
    std::string testInputFilePath_A_clone;
    std::string testInputFilePath_B;
    std::string testInputFilePath_A_oneDifference;
    void SetUp() {
        nonexistentPath = exeDir + "nonexistentPath.txt";
        testInputFilePath_A = exeDir + "../mockFiles/OSSpecificFileParser/UdpSocket.h";
        testInputFilePath_A_clone = exeDir + "../mockFiles/OSSpecificFileParser/UdpSocket_clone.h";
        testInputFilePath_A_oneDifference = exeDir + "../mockFiles/OSSpecificFileParser/UdpSocket_oneDifference.h";
        testInputFilePath_B = exeDir + "../mockFiles/OSSpecificFileParser/expected/_linux_UdpSocket.h";
    }
};

TEST_F(OSSpecificFileParser_compareTextFiles_test, noFile_ThrowException)
{
    // Both files don't exist.
    EXPECT_THROW(lcg::core::OSSpecificFileParser::compareTextFiles(nonexistentPath, nonexistentPath), std::runtime_error);
    // Either file don't exist.
    EXPECT_THROW(lcg::core::OSSpecificFileParser::compareTextFiles(nonexistentPath, testInputFilePath_A), std::runtime_error);
    EXPECT_THROW(lcg::core::OSSpecificFileParser::compareTextFiles(testInputFilePath_A, nonexistentPath), std::runtime_error);

    // Both files exist.
    EXPECT_NO_THROW(lcg::core::OSSpecificFileParser::compareTextFiles(testInputFilePath_A, testInputFilePath_A_clone));
    EXPECT_NO_THROW(lcg::core::OSSpecificFileParser::compareTextFiles(testInputFilePath_A_oneDifference, testInputFilePath_B));
}

TEST_F(OSSpecificFileParser_compareTextFiles_test, compareEqualFiles)
{
    int testCompareValue = lcg::core::OSSpecificFileParser::compareTextFiles(testInputFilePath_A, testInputFilePath_A_clone);
    int expectedTestCompareValue = 0;
    ASSERT_EQ(testCompareValue, expectedTestCompareValue);
}

TEST_F(OSSpecificFileParser_compareTextFiles_test, compareDifferentFiles_differentNumberOfLines)
{
    // Compare files with different number of lines.
    int testCompareValue = lcg::core::OSSpecificFileParser::compareTextFiles(testInputFilePath_A, testInputFilePath_B);
    int expectedTestCompareValue = -1;
    ASSERT_EQ(testCompareValue, expectedTestCompareValue);
}

TEST_F(OSSpecificFileParser_compareTextFiles_test, compareDifferentFiles_sameNumberOfLinesWithOneDifferentCharacter)
{
    // Compare files with equial number of lines, and just one difference in one character.
    int testCompareValue = lcg::core::OSSpecificFileParser::compareTextFiles(testInputFilePath_A, testInputFilePath_A_oneDifference);
    int expectedTestCompareValue = -1;
    ASSERT_EQ(testCompareValue, expectedTestCompareValue);
}

class OSSpecificFileParser_writeToPath_test : public ::testing::Test{
public:
    lcg::core::OSSpecificFileParser parser;
    std::string testInputFilePath;
    std::string testOutputFilePath;
    void SetUp() {
        testInputFilePath = exeDir + "../mockFiles/OSSpecificFileParser/UdpSocket.h";
        testOutputFilePath = exeDir + "../mockFiles/OSSpecificFileParser/UdpSocket_forgedClone.h";
    }
};

TEST_F(OSSpecificFileParser_writeToPath_test, createCloneOfFileAndCompareToOriginal)
{
    parser.setInputFilePath(testInputFilePath);
    parser.parse("clone");
    parser.writeToPath(testOutputFilePath);

    // Compare files as equal.
    int testCompareValue = lcg::core::OSSpecificFileParser::compareTextFiles(testInputFilePath, testOutputFilePath);
    int expectedTestCompareValue = 0;
    std::remove(testOutputFilePath.c_str());    // Delete temporary output file.

    ASSERT_EQ(testCompareValue, expectedTestCompareValue);
}

class OSSpecificFileParser_parseFile_test : public ::testing::Test{
public:
    lcg::core::OSSpecificFileParser parser;
    std::string nonexistentPath;
    std::string testInputFilePath;
    std::string testOutputFilePath;
    std::string windowsTestFileToComparePath;
    std::string linuxTestFileToComparePath;
    std::string undefinedString;
    std::string windowsString;
    std::string linuxString;
    void SetUp() {
        nonexistentPath = exeDir + "nonexistentPath.txt";
        testInputFilePath = exeDir + "../mockFiles/OSSpecificFileParser/UdpSocket.h";
        testOutputFilePath = exeDir + "../mockFiles/OSSpecificFileParser/expected/UdpSocket_outputFile.h";
        windowsTestFileToComparePath = exeDir + "../mockFiles/OSSpecificFileParser/expected/_windows_UdpSocket.h";
        linuxTestFileToComparePath = exeDir + "../mockFiles/OSSpecificFileParser/expected/_linux_UdpSocket.h";

        undefinedString = "undefined";
        windowsString = "__WINDOWS";
        linuxString = "__LINUX";
    }
    void TearDown(){
    }
};

TEST_F(OSSpecificFileParser_parseFile_test, noFile_ThrowException)
{
    parser.setInputFilePath(nonexistentPath);
    EXPECT_THROW(parser.parse(undefinedString), std::runtime_error);
}

TEST_F(OSSpecificFileParser_parseFile_test, openTestInputFiles)
{
    // Test general input file
    parser.setInputFilePath(testInputFilePath);
    EXPECT_NO_THROW(parser.parse("clone"));

    // Test windows output mock file
    parser.setInputFilePath(windowsTestFileToComparePath);
    EXPECT_NO_THROW(parser.parse("clone"));

    // Test linux output mock file
    parser.setInputFilePath(linuxTestFileToComparePath);
    EXPECT_NO_THROW(parser.parse("clone"));
}

TEST_F(OSSpecificFileParser_parseFile_test, unknownPlatformMacro_ThrowException)
{
    parser.setInputFilePath(testInputFilePath);
    parser.addPlatformMacro("__WINDOWS");
    EXPECT_THROW(parser.parse(linuxString), std::domain_error);
}

TEST_F(OSSpecificFileParser_parseFile_test, parseWindowsFile)
{
    parser.setInputFilePath(testInputFilePath);
    parser.addPlatformMacro("__WINDOWS");
    parser.addPlatformMacro("__LINUX");
    parser.parse(windowsString);
    parser.writeToPath(testOutputFilePath);

    // Compare parsed file with mock file
    int testCompareValue = lcg::core::OSSpecificFileParser::compareTextFiles(testOutputFilePath, windowsTestFileToComparePath);
    int expectedTestCompareValue = 0;
    std::remove(testOutputFilePath.c_str());    // Delete temporary output file.

    ASSERT_EQ(testCompareValue, expectedTestCompareValue);
}

TEST_F(OSSpecificFileParser_parseFile_test, parseLinuxFile)
{
    parser.setInputFilePath(testInputFilePath);
    parser.addPlatformMacro("__WINDOWS");
    parser.addPlatformMacro("__LINUX");
    parser.parse(linuxString);
    parser.writeToPath(testOutputFilePath);

    // Compare parsed file with mock file
    int testCompareValue = lcg::core::OSSpecificFileParser::compareTextFiles(testOutputFilePath, linuxTestFileToComparePath);
    int expectedTestCompareValue = 0;
    std::remove(testOutputFilePath.c_str());    // Delete temporary output file.

    ASSERT_EQ(testCompareValue, expectedTestCompareValue);
}
