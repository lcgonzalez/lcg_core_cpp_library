// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file UdpSocketTests.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.7.0
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the LCG Core Cpp Library.
//
//                          License Agreement
//                     For the LCG Core Cpp Library
//
//    LCG Core Cpp Library is a lightweight open source library used to wrap
//    pthreads, Berkeley sockets, and other C libraries and functionalities,
//    into C++ objectes, over Linux, Mac OS and Windows.
//
//    LCG Core Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    LCG Core Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include "lcg/core/UdpSocket.h"

#include <gtest/gtest.h>
#include <string>
#include <exception>

#if(1)
class UdpSocket_writeDatagram_Test : public ::testing::Test{
public:

    lcg::core::UdpSocket testSocket;
    lcg::core::UdpSocket assertingSocket;
    int assertingSocketBindingPort;
    int testSocketBindingPort;
    std::string localHost_;
    std::string multicastGroup_;
    std::string expectedData;
    char *actualData;

    void SetUp() {
        expectedData = "hello_world";
        actualData = new char[expectedData.size()+1];
        localHost_ = "127.0.0.1";
        multicastGroup_ = "239.255.42.101";
        testSocketBindingPort = 2000;
        assertingSocketBindingPort = 2001;

        testSocket.bind(localHost_, testSocketBindingPort, lcg::core::UdpSocket::ShareAddress);
        assertingSocket.bind(localHost_, assertingSocketBindingPort);
    }
    void TearDown(){
        delete[] actualData;
        testSocket.close();
        assertingSocket.close();
    }
};

TEST_F(UdpSocket_writeDatagram_Test, writeDatagramSizeInvalidArgument_ThrowException)
{
    EXPECT_THROW(testSocket.writeDatagram(expectedData.c_str(), UINT_MAX, localHost_, assertingSocketBindingPort), std::invalid_argument);
    EXPECT_NO_THROW(testSocket.writeDatagram(expectedData.c_str(), expectedData.size() + 1, localHost_, assertingSocketBindingPort));
}

TEST_F(UdpSocket_writeDatagram_Test, readDatagramSizeInvalidArgumetn_ThrowException)
{
    EXPECT_THROW(testSocket.readDatagram(actualData, UINT_MAX), std::invalid_argument);
}

TEST_F(UdpSocket_writeDatagram_Test, sendAndReceiveSimpleData_Unicast)
{
    try{
        testSocket.writeDatagram(expectedData.c_str(), expectedData.size()+1, localHost_, assertingSocketBindingPort);
        assertingSocket.readDatagram(actualData, expectedData.size()+1);
    } catch (std::runtime_error &e) {
        std::cout << e.what() << std::endl;
    }
    ASSERT_STREQ(expectedData.c_str(), actualData);
}

TEST_F(UdpSocket_writeDatagram_Test, readingTemeout_Unicast)
{
    int timeoutExpected = 0;
    try{
        assertingSocket.setReadingTimeout(2,0); // two seconds delay
        timeoutExpected = assertingSocket.readDatagram(actualData, expectedData.size()+1);
        assertingSocket.setReadingTimeout(0,0); // Return to defaults

        testSocket.writeDatagram(expectedData.c_str(), expectedData.size()+1, localHost_, assertingSocketBindingPort);
        assertingSocket.readDatagram(actualData, expectedData.size()+1);
    } catch (std::runtime_error &e) {
        std::cout << e.what() << std::endl;
    }
    ASSERT_STREQ(expectedData.c_str(), actualData);
    ASSERT_EQ(timeoutExpected, -2);
}

TEST_F(UdpSocket_writeDatagram_Test, readingTemeout_Multicast)
{
    int timeoutExpected = 0;
    try{
        testSocket.close();
        testSocket.joinMulticastGroup(multicastGroup_, "127.0.0.1", assertingSocketBindingPort-1);
        assertingSocket.close();
        assertingSocket.joinMulticastGroup(multicastGroup_, "127.0.0.1", assertingSocketBindingPort);

        assertingSocket.setReadingTimeout(2,0); // Two seconds delay
        timeoutExpected = assertingSocket.readDatagram(actualData, expectedData.size()+1);
         assertingSocket.setReadingTimeout(0,0); // Return to defaults

        testSocket.writeDatagram(expectedData.c_str(), expectedData.size()+1, multicastGroup_, assertingSocketBindingPort);
        assertingSocket.readDatagram(actualData, expectedData.size()+1);
    } catch (std::runtime_error &e) {
        std::cout << e.what() << std::endl;
    }
    ASSERT_STREQ(expectedData.c_str(), actualData);
    ASSERT_EQ(timeoutExpected, -2);
}

TEST_F(UdpSocket_writeDatagram_Test, sendAndReceiveSimpleData_Multicast)
{
    try{
        testSocket.close();
        testSocket.joinMulticastGroup(multicastGroup_, "127.0.0.1", assertingSocketBindingPort-1);
        assertingSocket.close();
        assertingSocket.joinMulticastGroup(multicastGroup_, "127.0.0.1", assertingSocketBindingPort);
        testSocket.writeDatagram(expectedData.c_str(), expectedData.size()+1, multicastGroup_, assertingSocketBindingPort);

        assertingSocket.readDatagram(actualData, expectedData.size()+1);
    } catch (std::runtime_error &e) {
        std::cout << e.what() << std::endl;
    }
    ASSERT_STREQ(expectedData.c_str(), actualData);
}

TEST_F(UdpSocket_writeDatagram_Test, readDatagramSenderAddress_Unicast)
{
    const char *expectedHostAddress = localHost_.c_str();
    int expectedHostPort = testSocketBindingPort;
    std::string actualHostAddress;
    int actualHostPort;

    try{
        testSocket.writeDatagram(expectedData.c_str(), expectedData.size()+1, localHost_, assertingSocketBindingPort);
        assertingSocket.readDatagram(actualData, expectedData.size()+1, actualHostAddress, &actualHostPort);
    } catch (std::runtime_error &e) {
        std::cout << e.what() << std::endl;
    }
    EXPECT_EQ(actualHostPort, expectedHostPort);
    ASSERT_STREQ(expectedHostAddress, actualHostAddress.c_str());
}

TEST_F(UdpSocket_writeDatagram_Test, readDatagramSenderAddress_Multicast)   // TODO: This test is failing on Josa's computer
{
    const char *expectedHostAddress = localHost_.c_str();
    int expectedHostPort = testSocketBindingPort;
    std::string actualHostAddress;
    int actualHostPort;

    try{
        testSocket.close();
        testSocket.joinMulticastGroup(multicastGroup_, "127.0.0.1", assertingSocketBindingPort-1);
        assertingSocket.close();
        assertingSocket.joinMulticastGroup(multicastGroup_, "127.0.0.1", assertingSocketBindingPort);
        testSocket.writeDatagram(expectedData.c_str(), expectedData.size()+1, multicastGroup_, assertingSocketBindingPort);

        assertingSocket.readDatagram(actualData, expectedData.size()+1, actualHostAddress, &actualHostPort);
    } catch (std::runtime_error &e) {
        std::cout << e.what() << std::endl;
    }
    EXPECT_EQ(actualHostPort, expectedHostPort);
    ASSERT_STREQ(expectedHostAddress, actualHostAddress.c_str());
}

class UdpSocket_binding_Test : public ::testing::Test{
public:

    lcg::core::UdpSocket testSocket;

    void SetUp() {

    }
    void TearDown(){

    }
};

TEST_F(UdpSocket_binding_Test, bindBadIPAddress_ThrowException)
{
    EXPECT_THROW(testSocket.bind("1.0.0.1"), std::runtime_error);
    EXPECT_NO_THROW(testSocket.bind("0.0.0.0"));
}

TEST_F(UdpSocket_binding_Test, bindMoreThanOnce_ThrowException)
{
    EXPECT_NO_THROW(testSocket.bind("127.0.0.1"));
    EXPECT_THROW(testSocket.bind("127.0.0.1"), std::runtime_error);
    EXPECT_THROW(testSocket.bind("0.0.0.0"), std::runtime_error);
}

TEST_F(UdpSocket_binding_Test, bindCloseAndBindAgain_ThrowException)
{
    EXPECT_NO_THROW(testSocket.bind("0.0.0.0"));
    EXPECT_THROW(testSocket.bind("127.0.0.1"), std::runtime_error);
    testSocket.close();
    EXPECT_NO_THROW(testSocket.bind("127.0.0.1"));
}

TEST_F(UdpSocket_binding_Test, bindBadStringAddress_ThrowException)
{
    EXPECT_THROW(testSocket.bind("example.com"), std::invalid_argument);
    EXPECT_NO_THROW(testSocket.bind("127.0.0.1"));
}
#endif
