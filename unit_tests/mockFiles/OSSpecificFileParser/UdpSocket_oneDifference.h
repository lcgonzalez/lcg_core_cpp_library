// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file UdpSocket.h
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.4.0
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the LCG Core Cpp Library.
//
//                          License Agreement
//                     For the LCG Core Cpp Library
//
//    LCG Core Cpp Library is a lightweight open source library used to wrap
//    pthreads, Berkeley sockets, and other C libraries and functionalities,
//    into C++ objectes, over Linux, Mac OS and Windows.
//
//    LCG Core Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    LCG Core Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#ifndef LCGUDPSOCKET_H
#define LCGUDPSOCKET_H

#include "lcg/core/LCG_Core.h"

#ifdef __WINDOWS
#include <winsock2.h>   // scoket
#include <ws2tcpip.h>   // socket
//#pragma comment(lib, "ws2_32.lib") // Add library to Visual Studio Project
#endif // __WINDOWS
#ifdef __LINUX
#include <sys/types.h>  // bind (For compability and protability)
#include <sys/socket.h> // bind
#include <netinet/in.h> // sockaddr_in, in_addr, INET_ADDRSTRLEN
//#include <netinet/ip.h> // superset of previous
#include <arpa/inet.h>  // inet_ntop, inet_pton, ntohs, htons
#include <netdb.h>      // NI_NUMERICSERV
#include <cerrno>       // errno
#include <cstring>      // std::strerror
#include <unistd.h>     // close()
#endif // __LINUX

#include <string>       // std::string
#include <climits>      // INT_MAX
#include <stdexcept>    // std::runtime_error

namespace lcg{
namespace core{

#define TRYAGAIN -2 // Try to read a datagram again

/**
 * @brief The UdpSocket class creates a UDP socket to send data in unicast or multicast mode.
 * The socket is easily managed by the bind, joinMulticastGroup, writeDatagram
 * readDatagram and close methods.
 * @see UdpSocket::bind, UdpSocket::joinMulticastGroup, UdpSocket::writeDatagram,
 * UdpSocket::readDatagram, UdpSocket::close.
 */
class LCG_CORE_LIB_API UdpSocket {
public:
    /**
     * @brief UdpSocket constructor that creates a IPv4 UDP socket ready to be bound.
     * If the socket would be used for reading incoming data packets, the method
     * bind should be called before readDatagram.
     * If the socked would only be used for writing outgoing data packets,
     * the method writeDatagram may be called without a previous call to bind.
     * @see bind, readDatagram, writeDatagram
     */
    UdpSocket();

    /**
     * @brief ~UdpSocket destructor that unbinds and close the socket.
     * @see close.
     */
    ~UdpSocket();

    /**
     * @brief The BindMode enum defines the socket behaviour.
     * This variable defines if the socket would be unique (DontShareAddress),
     * shared (ShareAddress), broadcast (SendBroadcast) or default for platform
     * (DefaultForPlaform).
     * @see bind, setSocketOptions.
     */
    enum BindMode {
        DefaultForPlatform = 0x0, /**< The default option for the current platform.
            * On Unix and Mac OS X, this is equivalent to DontShareAddress,
            * and on Windows, its equivalent to ShareAddress.
            */
        ShareAddress = 0x1,     /**< Allow other services to bind to the same address and port. */
        DontShareAddress = 0x2, /**< Bind the address and port exclusively, so that
            * no other services are allowed to rebind.
            */
        SendBroadcast = 0x4     /**< Configures socket for sending broadcast data. */
    };

    /**
     * @brief bind Binds socket to sZAddress:port using a binding mode.
     * Binds to specific address. This operation should be performed before
     * reading any datagram using the method readDatagram.
     * However, if only sending a datagram, previous calls to this method can
     * be skipped and direct use of writeDatagram is allowed.
     * If binding to an already bound socket, an exception will be rised, so the
     * socket should be closed before a new bind is attempted.
     * If the socket is closed, this method would attempt to create a new socket
     * and bind to the specific address and port.
     *
     * @param szAddress String that represents the IPv4 binding address. In
     * combination with the port, they represent the full qualified binding
     * address.
     * If using the special value szAddress="0.0.0.0", the socket will receive
     * all the traffic that arrives to the computer, without taking into account
     * the sender's ip address.
     * @param port Integer that represents the binding port. In combination with
     * the szAddress, they represent the full qualified binding address.
     * If left port=0, the socket will receive all the traffic that comes from
     * the binding address asAddress.
     * @param bindMode Defines the socket behaviour, whether it would be unique,
     * shared or broadcast. See BindMode.
     * @return 0 on success. On failure it rises an exception of type std::domain_error
     * @see BindMode enum, readDatagram, close.
     */
    int bind(std::string szAddress, int port=0, BindMode bindMode=DefaultForPlatform);

    /**
     * @brief setSocketOptions Defines the behaviour of the socket.
     * The socket behaviour can be defined to be a unique socket (bind address
     * can't be used again), shared socket (bind address can be used again),
     * broadcast (socket sends broadcast data) or defined by the platform.
     * @param newBindMode Used to define the socket behaviour.
     * @see BindMode enum, bind.
     * @return O on success. On failure it rises an exception of type std::domain_error
     */
    int setSocketOptions(BindMode newBindMode);

    /**
     * @brief setReadingTimeout Sets the timeout used by readDatagram method before returning (non blocking).
     * This time is used by the readDatagram method, before returning with a
     * TRYAGAIN value. If sec=0 and usec=0, readDatagram would be trated as a
     * blocking method, indefinitely blocking the thread execution until data
     * arrives to the binding address and port.
     * Note, not all implementations allow this option to be set. Usually, the usec variable
     * is ignored by the platform.
     * @param sec Seconds to timeout.
     * @param usec Micro seconds to timeout.
     * @return O on success. On failure it rises an exception of type std::domain_error
     * @see readDatagram.
     */
    int setReadingTimeout(unsigned int sec, unsigned int usec);

    /**
     * @brief joinMulticastGroup Joins to a multicast group without previos call to bind.
     * This method DOES NOT requiere a bound socket, so a previous call to bind
     * method is not requiered. All the binding will be carried out by this method.
     * Trying to join to a bound socket will result in an exception rised.
     * @param szGroupAddress String form of the IPv4 multicast group address.
     * @param szInterfaceAddress String form of the IPv4 interface address of
     * the local machine. This address defines the interfac (network) that this
     * socket will be joining.
     * @param port Port of the multicast group.
     * @return True on success. On failure it rises an exception of type std::domain_error
     */
    bool joinMulticastGroup(std::string szGroupAddress, std::string szInterfaceAddress, int port);

    /**
     * @brief writeDatagram Sends data to the desired address and port.
     * The socket doesn't need to be in a bound state (previous call to bind),
     * to perform this operation.
     * @param data Pointer to buffer containing the data desired to be sent.
     * @param dataSize Number of bytes to send from the data buffer. This
     * number can't exceed INT_MAX (platform dependant), or an exception of
     * type std::overflow_error will be rised.
     * @param szAddress String form of the IPv4 destination addres where the
     * data will be sent.
     * @param port Port of the destination address.
     * @return Number of data sent to the address on success. On failure it
     * rises an exception of type std::domain_error or std::overflow_error.
     * @see UdpSocket
     */
    int writeDatagram(const char *data, size_t dataSize, const std::string szAddress, int port);

    /**
     * @brief readDatagram Reads data from the binding address and port.
     * The socket DOES NEED to be in a bound state (previous call to bind),
     * before a call to this method.
     * @param data Pointer to buffer where the incoming data will be stored.
     * @param maxSize Number of bytes to read into the data buffer. This number
     * can't exceed INT_MAX (platform dependant), or an excpetion of type
     * std::overflow_error will be rised.
     * @param hostAddress IPv4 address in string form of the remote host machine
     * that sent the data.
     * @param port Port of the remote host machine that sent the data.
     * @return 0 on success, TRYAGAIN if a timeout ocurred. On failure it
     * rises an exception of type std::domain_error or std::overflow_error.
     * @see bind, setReadingTimeout.
     */
    int readDatagram(char *data, size_t maxSize, std::string &hostAddress, int *port);

    /**
     * @brief readDatagram Same as readDatagram, but without getting the IPv4 address and port of the remote host machine.
     * The socket DOES NEED to be in a bound state (previous call to bind),
     * before a call to this method.
     * @param data Pointer to buffer where the incoming data will be stored.
     * @param maxSize Number of bytes to read into the data buffer. This number
     * can't exceed INT_MAX (platform dependant), or an excpetion of type
     * std::overflow_error will be rised.
     * @return 0 on success, TRYAGAIN if a timeout ocurred. On failure it
     * rises an exception of type std::domain_error or std::overflow_error.
     * @see bind, setReadingTimeout.
     */
    int readDatagram(char *data, size_t maxSize);

    /**
     * @brief close Unbinds and closes the socket.
     * This is automatically called by the destructor, so no need to call it
     * unless wanting to rebind to other address/group.
     * @see ~UdpSocket
     */
    void close();

private:
    /**
     * @brief The SocketState enum Describes the state of the socket.
     * The socket coud be bound (BoundState), unbound (UnboundState) or
     * cosed (Closed).
     * @see
     */
    enum SocketState {
        UnboundState = 0x0, /**< The socket is not bound. */
        BoundState = 0x1,   /**< The socket is bound to an address and port. */
        Closed = 0x2        /**< The socket is closed. */
    };

    void createSocket();
    SocketState socketState_;

    /**
     * @brief joinMulticastGroup Joins to a multicast group requiring a previous call to bind.
     * This method requieres a bound socket, so a previous call to bind, with the
     * groupAddress as the binding address, is requiered.
     * @param szGroupAddress String form of the IPv4 multicast group address.
     * @param szInterfaceAddress String form of the IPv4 interface address of the
     * local machine, that defines the interface (or network) that this socket
     * will be joining.
     * @return True on success. On failure it rises an exception of type std::domain_error
     */
    bool joinMulticastGroup(std::string szGroupAddress, std::string szInterfaceAddress);

    // convert ipp address string to addr
    bool IPAddressStringToAddr(const std::string szNameOrAddress, struct in_addr *Address);
    bool timeOutError();
    bool checkForSysInterruptionRecall(int errorCode);

    sockaddr_in destination;
    #ifdef __LINUX
    int socketID_;
    #endif // __LINUX
    #ifdef __WINDOWS
    SOCKET socketID_;
    #endif // __WINDOWS

    /////////////////////////////// EXCEPTIONS /////////////////////////////////
    std::domain_error bind_SocketAlreadyBound_Exception();
    std::domain_error bind_SocketNotBound_Exception(int errorCode);
    std::domain_error createSocket_InvalidSocket_Exception(int errorCode);
    std::domain_error IPAddressStringToAddr_AddrNotDefined_Exception(int errorCode);
    std::domain_error setSocketOptions_setsockoptError_Exception(BindMode bindMode,
                                                                  int errorCode);
    std::domain_error setReadingTimeout_setsockoptError_Exception(unsigned int sec,
                                                                   unsigned int usec,
                                                                   int errorCode);
    std::domain_error joinMulticastGroup_JoinGroupError_Exception(std::string groupAddress,
                                                                   int errorCode);
    std::domain_error joinMulticastGroup_SetLocalInerfaceError_Exception(std::string interfaceAddress,
                                                                          int errorCode);
    std::overflow_error writeDatagram_overflowError_Exception();
    std::domain_error writeDatagram_sendigDatagramError_Exception(int errorCode);
    std::overflow_error readDatagram_overflowError_Exception();
    std::domain_error readDatagram_ReadingDatagramError_Exception(int errorCode);
    #ifdef __WINDOWS
    std::domain_error createSocket_WSAerror_Exception(int WSAErrorCode);
    #endif // __WINDOWS
};

} // namespace core
} // namespace lcg

#endif // LCGUDPSOCKET_H
