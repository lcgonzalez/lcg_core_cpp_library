// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file shared_ptrTests.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.7.0
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the LCG Core Cpp Library.
//
//                          License Agreement
//                     For the LCG Core Cpp Library
//
//    LCG Core Cpp Library is a lightweight open source library used to wrap
//    pthreads, Berkeley sockets, and other C libraries and functionalities,
//    into C++ objectes, over Linux, Mac OS and Windows.
//
//    LCG Core Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    LCG Core Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include "lcg/core/memory.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "lcg/core/UdpSocket.h" // lcg::core::UdpSocket
#include "lcg/core/Version.h"   // lcg::core::Version

class shared_ptr_test : public ::testing::Test {
public:
    void makeCopiesOfSharedPointer(lcg::core::shared_ptr<int> ptr) {
        lcg::core::shared_ptr<int> ptr2 = ptr;
        lcg::core::shared_ptr<int> ptr3;
        ptr3 = ptr;
    }


    void SetUp() {

    }
    void TearDown() {

    }
};

TEST_F(shared_ptr_test, constructElementalPointerAndUseIt){
    lcg::core::shared_ptr<int> ptr(new int);
    int expected1 = 123;
    *ptr=123;   // rvalue assignment
    ASSERT_EQ(expected1, *ptr);

    int expected2 = 321;
    *ptr=expected2; // lvalue assignment
    ASSERT_EQ(expected2, *ptr);
}

TEST_F(shared_ptr_test, assignConstructElementalPointerAndUseIt){
    lcg::core::shared_ptr<int> ptr = new int;
    int expected1 = 123;
    *ptr=123;   // rvalue assignment
    ASSERT_EQ(expected1, *ptr);

    int expected2 = 321;
    *ptr=expected2; // lvalue assignment
    ASSERT_EQ(expected2, *ptr);
}

TEST_F(shared_ptr_test, dereferencePointerUsingArrowOperator){
    lcg::core::shared_ptr<std::vector<int> > ptr = new std::vector<int>;
    std::vector<int> expected;
    expected.push_back(1);
    expected.push_back(2);
    expected.push_back(3);
    ptr->push_back(1);
    ptr->push_back(2);
    ptr->push_back(3);
    ASSERT_THAT(expected, ::testing::ContainerEq(*ptr));
}

TEST_F(shared_ptr_test, attemptToDereferenceNullPointer_throwException) {
    lcg::core::shared_ptr<std::vector<int> > ptr;
    EXPECT_THROW(*ptr, std::runtime_error);
    EXPECT_THROW(ptr->push_back(19), std::runtime_error);
}

TEST_F(shared_ptr_test, invalidatePointerThroughNewAssignment) {
    int* pInt1 = new int;
    lcg::core::shared_ptr<int> ptr;
    ptr = pInt1;
    ASSERT_EQ(ptr, pInt1);
    int expected1 = 23;
    *ptr = expected1;
    ASSERT_EQ(*ptr, expected1);

    int* pInt2 = new int;
    ptr = pInt2;    // Implicit deleting of pInt1.
    ASSERT_EQ(ptr, pInt2);
    int expected2 = 32;
    *ptr = expected2;
    ASSERT_EQ(*ptr, expected2);
}

TEST_F(shared_ptr_test, makeSeveralCopiesOfSamePointer) {
    int* pInt1 = new int;
    lcg::core::shared_ptr<int> ptr = pInt1;

    this->makeCopiesOfSharedPointer(ptr);

    ASSERT_EQ(ptr, pInt1);
    int expected1 = 23;
    *ptr = expected1;
    ASSERT_EQ(*ptr, expected1);

    int* pInt2 = new int;
    ptr = pInt2;    // Implicit deleting of pInt1.
    ASSERT_EQ(ptr, pInt2);
    int expected2 = 32;
    *ptr = expected2;
    ASSERT_EQ(*ptr, expected2);
}

TEST_F(shared_ptr_test, constructAndAssignPointerToDerivedClassAndUseIt) {
    // Create pointers to objects of derived class (lcg::core::UdpSocket) of base class family (lcg::core::Core)
    lcg::core::UdpSocket* pUdpSocket1 = new lcg::core::UdpSocket;
    lcg::core::shared_ptr<lcg::core::UdpSocket> ptr01_1 = pUdpSocket1;  // Derived constructor given derived raw pointer
    lcg::core::shared_ptr<lcg::core::Core> ptr01_2 = pUdpSocket1;       // Base constructor given derived raw pointer
    lcg::core::shared_ptr<lcg::core::Core> ptr01_3;
    ptr01_3 = pUdpSocket1;                                              // Base assignment operator given derived raw pointer
    ASSERT_EQ(ptr01_1, pUdpSocket1);
    ASSERT_EQ(ptr01_2, pUdpSocket1);
    ASSERT_EQ(ptr01_3, pUdpSocket1);

    lcg::core::UdpSocket* pUdpSocket2 = new lcg::core::UdpSocket;
    lcg::core::shared_ptr<lcg::core::UdpSocket> ptr02_1;
    ptr02_1 = pUdpSocket2;                                          // Derived assignment operator given derived raw pointer
    lcg::core::shared_ptr<lcg::core::Core> ptr02_2;
    ptr02_2 = pUdpSocket2;                                          // Base assignment operator given derived raw pointer
    lcg::core::shared_ptr<lcg::core::Core> ptr02_3 = pUdpSocket2;   // Base constructor given derived raw pointer
    ASSERT_EQ(ptr02_1, pUdpSocket2);
    ASSERT_EQ(ptr02_2, pUdpSocket2);
    ASSERT_EQ(ptr02_3, pUdpSocket2);

    lcg::core::shared_ptr<lcg::core::Core> ptr01_6 = ptr01_1;       // Base constructor given derived shared_ptr
    lcg::core::shared_ptr<lcg::core::Core> ptr01_7;
    ptr01_7 = ptr01_1;                                              // Base assignment operator given derived shared_ptr

    lcg::core::shared_ptr<lcg::core::UdpSocket> ptr02_6 = ptr02_2;  // Derived constructor given base shared_ptr
    lcg::core::shared_ptr<lcg::core::UdpSocket> ptr02_7;
    ptr02_7 = ptr02_2;                                              // Derived assignment operator given base shared_ptr

    ASSERT_EQ(ptr01_6, pUdpSocket1);
    ASSERT_EQ(ptr01_7, pUdpSocket1);
    ASSERT_EQ(ptr02_6, pUdpSocket2);
    ASSERT_EQ(ptr02_7, pUdpSocket2);

    // Create pointers to objects of other derived class (lcg::core::Version) of the same class family (lcg::core::Core)
    lcg::core::Version* pVersion1 = new lcg::core::Version;
    lcg::core::shared_ptr<lcg::core::Core> ptr03_1 = pVersion1;     // Base constructor given derived raw pointer
    lcg::core::shared_ptr<lcg::core::Version> ptr03_2 = pVersion1;  // Derived constructor given derived raw pointer
    lcg::core::shared_ptr<lcg::core::Core> ptr03_3;
    ptr03_3 = pVersion1;                                            // Base assignment operator given derived raw pointer
    lcg::core::shared_ptr<lcg::core::Version> ptr03_4;
    ptr03_4 = pVersion1;                                            // Derived assignment operator given derived raw pointer
    ASSERT_EQ(ptr03_1, pVersion1);
    ASSERT_EQ(ptr03_2, pVersion1);
    ASSERT_EQ(ptr03_3, pVersion1);
    ASSERT_EQ(ptr03_4, pVersion1);

    lcg::core::Version* pVersion2 = new lcg::core::Version;
    lcg::core::shared_ptr<lcg::core::Core> ptr04_1 = pVersion2;     // Base constructor given derived raw pointer
    lcg::core::shared_ptr<lcg::core::Version> ptr04_2 = pVersion2;  // Derived constructor given derived raw pointer
    lcg::core::shared_ptr<lcg::core::Core> ptr04_3;
    ptr04_3 = pVersion2;                                            // Base assignment operator given derived raw pointer
    lcg::core::shared_ptr<lcg::core::Version> ptr04_4;
    ptr04_4 = pVersion2;                                            // Derived assignment operator given derived raw pointer
    ASSERT_EQ(ptr04_1, pVersion2);
    ASSERT_EQ(ptr04_2, pVersion2);
    ASSERT_EQ(ptr04_3, pVersion2);
    ASSERT_EQ(ptr04_4, pVersion2);

    // Create pointers to objects of base class (lcg::core::Core)
    lcg::core::Core* pCore1 = new lcg::core::Core;
    lcg::core::shared_ptr<lcg::core::Core> ptr05_1 = pCore1;        // Base constructor given base raw pointer
    lcg::core::shared_ptr<lcg::core::Core> ptr05_2 = ptr05_1;       // Base constructor given base shared_ptr
    lcg::core::shared_ptr<lcg::core::Core> ptr05_3;
    ptr05_3 = pCore1;                                               // Base assignment operator given base raw pointer
    lcg::core::shared_ptr<lcg::core::Core> ptr05_4;
    ptr05_4 = ptr05_1;                                              // Base assignment operator given base shared_ptr
    ASSERT_EQ(ptr05_1, pCore1);
    ASSERT_EQ(ptr05_2, pCore1);
    ASSERT_EQ(ptr05_3, pCore1);
    ASSERT_EQ(ptr05_4, pCore1);

    lcg::core::Core* pCore2 = new lcg::core::Core;
    lcg::core::shared_ptr<lcg::core::Core> ptr06_1;
    ptr06_1 = pCore2;                                               // Base assignment operator given base raw pointer
    lcg::core::shared_ptr<lcg::core::Core> ptr06_2;
    ptr06_2 = ptr06_1;                                              // Base assignment operator given base shared_ptr
    lcg::core::shared_ptr<lcg::core::Core> ptr06_3 = pCore2;        // Base constructor given base raw pointer
    lcg::core::shared_ptr<lcg::core::Core> ptr06_4 = ptr06_1;       // Base constructor given base shared_ptr
    ASSERT_EQ(ptr06_1, pCore2);
    ASSERT_EQ(ptr06_2, pCore2);
    ASSERT_EQ(ptr06_3, pCore2);
    ASSERT_EQ(ptr06_4, pCore2);

    // Create pointers to objects of other base class (std::exception)
    std::exception* pException1 = new std::exception;
    lcg::core::shared_ptr<std::exception> ptr07_1 = pException1;
    lcg::core::shared_ptr<std::exception> ptr07_2 = ptr07_1;
    ASSERT_EQ(ptr07_1, pException1);
    ASSERT_EQ(ptr07_2, pException1);

    std::exception* pException2 = new std::exception;
    lcg::core::shared_ptr<std::exception> ptr08_1 = pException2;
    lcg::core::shared_ptr<std::exception> ptr08_2 = ptr08_1;
    ASSERT_EQ(ptr08_1, pException2);
    ASSERT_EQ(ptr08_2, pException2);

    // Create pointers to objects of other derived class (std::runtime_error) of other base class family (std::exception)
    std::runtime_error* pRuntime_error1 = new std::runtime_error("");
    lcg::core::shared_ptr<std::exception> ptr09_1 = pRuntime_error1;
    lcg::core::shared_ptr<std::runtime_error> ptr09_2 = pRuntime_error1;
    lcg::core::shared_ptr<std::exception> ptr09_3;
    ptr09_3 = pRuntime_error1;
    lcg::core::shared_ptr<std::runtime_error> ptr09_4;
    ptr09_4 = ptr09_2;
    ASSERT_EQ(ptr09_1, pRuntime_error1);
    ASSERT_EQ(ptr09_2, pRuntime_error1);
    ASSERT_EQ(ptr09_3, pRuntime_error1);
    ASSERT_EQ(ptr09_4, pRuntime_error1);

    std::runtime_error* pRuntime_error2 = new std::runtime_error("");
    lcg::core::shared_ptr<std::exception> ptr10_1 = pRuntime_error2;
    lcg::core::shared_ptr<std::runtime_error> ptr10_2 = pRuntime_error2;
    lcg::core::shared_ptr<std::exception> ptr10_3 ;
    ptr10_3 = pRuntime_error2;
    lcg::core::shared_ptr<std::runtime_error> ptr10_4;
    ptr10_4 = ptr10_1;
    lcg::core::shared_ptr<std::exception> ptr10_5 = dynamic_cast<std::exception*>(pRuntime_error2);
    lcg::core::shared_ptr<std::runtime_error> ptr10_6 = dynamic_cast<std::exception*>(pRuntime_error2);
    ASSERT_EQ(ptr10_1, pRuntime_error2);
    ASSERT_EQ(ptr10_2, pRuntime_error2);
    ASSERT_EQ(ptr10_3, pRuntime_error2);
    ASSERT_EQ(ptr10_4, pRuntime_error2);
    ASSERT_EQ(ptr10_5, pRuntime_error2);
    ASSERT_EQ(ptr10_6, pRuntime_error2);

    // Create pointer to objects of derived class (std::runtime_error), using a pointer to base class (std::exception)
    std::exception* pRuntime_error3 = new std::runtime_error("");
    lcg::core::shared_ptr<std::exception> ptr11_1 = pRuntime_error3;
    lcg::core::shared_ptr<std::runtime_error> ptr11_2 = pRuntime_error3;
    lcg::core::shared_ptr<std::exception> ptr11_3 = pRuntime_error3;
    lcg::core::shared_ptr<std::runtime_error> ptr11_4 = pRuntime_error3;
    lcg::core::shared_ptr<std::exception> ptr11_5 = dynamic_cast<std::runtime_error*>(pRuntime_error3);
    lcg::core::shared_ptr<std::runtime_error> ptr11_6 = dynamic_cast<std::runtime_error*>(pRuntime_error3);
    ASSERT_EQ(ptr11_1, pRuntime_error3);
    ASSERT_EQ(ptr11_2, pRuntime_error3);
    ASSERT_EQ(ptr11_3, pRuntime_error3);
    ASSERT_EQ(ptr11_4, pRuntime_error3);
    ASSERT_EQ(ptr11_5, pRuntime_error3);
    ASSERT_EQ(ptr11_6, pRuntime_error3);

    // Example of not manually casting base/derived pointers, delegate that task to the shared_ptr constuctors and assignment operator
    std::exception* pDomain_error1 = new std::domain_error("");
    lcg::core::shared_ptr<std::domain_error> ptr12_1 = pDomain_error1;
    // The next line will cause an error (freeing the same pointer twice) because raw pointers polymorphism assignment is not
    // supported by shared_ptr. In order to obtain the desaired behaviour, use the smart pointer constructors and assignment operator
    // instead of casting raw pointer.
    //lcg::core::shared_ptr<std::logic_error> ptr12_2 = dynamic_cast<std::logic_error*>(pDomain_error1);  // Error
    lcg::core::shared_ptr<std::logic_error> ptr12_3 = pDomain_error1;                                   // OK
    lcg::core::shared_ptr<std::logic_error> ptr12_4 = ptr12_1;                                          // OK
    //lcg::core::shared_ptr<std::exception> ptr12_5 = dynamic_cast<std::exception*>(pDomain_error1);      // Error
    lcg::core::shared_ptr<std::exception> ptr12_5 = pDomain_error1;                                     // OK
    lcg::core::shared_ptr<std::exception> ptr12_7 = ptr12_1;                                            // OK
    ASSERT_EQ(ptr12_1, pDomain_error1);
    ASSERT_EQ(ptr12_3, pDomain_error1);
    ASSERT_EQ(ptr12_4, pDomain_error1);
    ASSERT_EQ(ptr12_5, pDomain_error1);
    ASSERT_EQ(ptr12_7, pDomain_error1);
}

TEST_F(shared_ptr_test, invalidatePointerThroughNewAssignment_templateCovariance) {
    // Assign multiple times raw pointers of the same class
    std::exception* pException1 = new std::exception;
    std::exception* pException2 = new std::exception;
    std::exception* pException3 = new std::exception;
    std::exception* pException4 = new std::exception;
    lcg::core::shared_ptr<std::exception> ptr01_1 = pException1;
    lcg::core::shared_ptr<std::exception> ptr02_1 = pException1;
    lcg::core::shared_ptr<std::exception> ptr03_1 = ptr01_1;
    ptr01_1 = pException1;
    ptr02_1 = pException2;
    ptr03_1 = pException3;
    ptr01_1 = pException4;  // pException1 destroyed
    ASSERT_EQ(ptr01_1, pException4);
    ASSERT_EQ(ptr02_1, pException2);
    ASSERT_EQ(ptr03_1, pException3);

    // Assign multiple times raw pointers of different class but same class family (derived to base class)
    std::domain_error* pDomain_error1 = new std::domain_error("");
    std::exception* pDomain_error2 = new std::domain_error("");
    std::logic_error* pDomain_error3 = new std::domain_error("");
    lcg::core::shared_ptr<std::exception> ptr04_1 = pDomain_error1;
    lcg::core::shared_ptr<std::domain_error> ptr05_1 = pDomain_error2;
    lcg::core::shared_ptr<std::exception> ptr06_1 = pDomain_error3;
    lcg::core::shared_ptr<std::logic_error> ptr07_1;
    ptr07_1 = ptr04_1;
    ASSERT_EQ(ptr04_1, pDomain_error1);
    ASSERT_EQ(ptr05_1, pDomain_error2);
    ASSERT_EQ(ptr06_1, pDomain_error3);
    ASSERT_EQ(ptr07_1, ptr04_1);

    std::exception* pOtherException1 = new std::exception;
    std::exception* pOtherException2 = new std::domain_error("");
    std::exception* pOtherException3 = new std::logic_error("");
    lcg::core::shared_ptr<std::exception> ptr08_1 = pOtherException1;
    lcg::core::shared_ptr<std::exception> ptr09_1 = pOtherException2;
    lcg::core::shared_ptr<std::exception> ptr10_1 = pOtherException3;
    ptr10_1 = ptr08_1;  // pOtherException3 destroyed
    ptr08_1 = ptr09_1;
    ptr09_1 = ptr10_1;
    ASSERT_EQ(ptr08_1, pOtherException2);
    ASSERT_EQ(ptr09_1, pOtherException1);
    ASSERT_EQ(ptr10_1, pOtherException1);

    // Assign multiple times shared_ptr of the same class
    std::exception* pAnotherException1 = new std::exception;
    std::exception* pAnotherException2 = new std::exception;
    std::exception* pAnotherException3 = new std::exception;
    lcg::core::shared_ptr<std::exception> ptr11_1 = pAnotherException1;
    lcg::core::shared_ptr<std::exception> ptr12_1 = pAnotherException2;
    lcg::core::shared_ptr<std::exception> ptr13_1 = pAnotherException3;
    lcg::core::shared_ptr<std::exception> ptr14_1;
    ptr14_1 = ptr11_1;
    ptr11_1 = ptr12_1;
    ptr12_1 = ptr14_1;
    ptr14_1 = ptr13_1;
    ASSERT_EQ(ptr11_1, pAnotherException2);
    ASSERT_EQ(ptr12_1, pAnotherException1);
    ASSERT_EQ(ptr13_1, pAnotherException3);
    ASSERT_EQ(ptr14_1, pAnotherException3);

    // Assign multiple times shared_ptr of different class but same class family (derived to base class)
    std::domain_error* pOtherDomain_error1 = new std::domain_error("");
    std::exception* pOtherDomain_error2 = new std::domain_error("");
    std::logic_error* pOtherDomain_error3 = new std::domain_error("");
    lcg::core::shared_ptr<std::exception> ptr16_1 = pOtherDomain_error1;
    lcg::core::shared_ptr<std::domain_error> ptr17_1 = pOtherDomain_error2;
    lcg::core::shared_ptr<std::exception> ptr18_1 = pOtherDomain_error3;
    lcg::core::shared_ptr<std::domain_error> ptr19_1 = pOtherDomain_error2;
    ptr18_1 = ptr16_1;  // pOtherDomain_error3 destroyed
    ptr16_1 = ptr17_1; // shared_ptr to base now holds shared_ptr to derived that was constructed using derived object from a pointer to base
    ptr17_1 = ptr18_1;  // shared_ptr to derived now holds shared_ptr to base that was assigned a shared_ptr to base that was constructed using a derived object form a pointer to base
    ptr19_1 = ptr16_1;
    ASSERT_EQ(ptr16_1, pOtherDomain_error2);
    ASSERT_EQ(ptr17_1, pOtherDomain_error1);
}

TEST_F(shared_ptr_test, badCast_throwException) {
    // Base class raw pointer to derived shared_ptr
    std::exception* pException1 = new std::exception;
    lcg::core::shared_ptr<std::domain_error> ptr01_1;
    EXPECT_THROW(ptr01_1 = pException1, std::runtime_error);
    //delete pException1; // Error, the pException1 memory is freed althought the exception is thrown

    // Base class shared_ptr to derived shared_ptr
    std::exception* pException2 = new std::exception;
    lcg::core::shared_ptr<std::exception> ptr02_1 = pException2;
    EXPECT_THROW(ptr01_1 = ptr02_1, std::runtime_error);

    // Unrelated raw pointer to shared_ptr
    std::exception* pException3 = new std::exception;
    lcg::core::shared_ptr<lcg::core::UdpSocket> ptr03_1;
    EXPECT_THROW(ptr03_1 = pException3, std::runtime_error);
    //delete pException3; // Error, the pException1 memory is freed althought the exception is thrown

    // Unrelated shared_ptr to shared_ptr
    std::exception* pException4 = new std::domain_error("");
    lcg::core::shared_ptr<std::exception> ptr04_1 = pException4;
    lcg::core::shared_ptr<lcg::core::UdpSocket> ptr05_1;
    EXPECT_THROW(ptr05_1 = ptr04_1, std::runtime_error);
}

TEST_F(shared_ptr_test, badComparisons_throwException) {
    // Comparison between shared_ptr and raw pointer of not related classes
    std::exception* pException1 = new std::exception;
    lcg::core::shared_ptr<std::exception> ptr01_1 = pException1;
    lcg::core::UdpSocket* pUdpSocket1 = new lcg::core::UdpSocket;
    EXPECT_THROW(ptr01_1 == pUdpSocket1, std::runtime_error);
    //delete pUdpSocket1; // Error, the pUdpSocket1 memory is freed althought the exception is thrown

    // Comparison between shared_ptr and shared_ptr of not related classes
    std::exception* pException2 = new std::exception;
    lcg::core::shared_ptr<std::exception> ptr02_1 =  pException2;
    lcg::core::UdpSocket* pUdpSocket2 = new lcg::core::UdpSocket;
    lcg::core::shared_ptr<lcg::core::UdpSocket> ptr03_1 = pUdpSocket2;
    EXPECT_THROW(ptr02_1 == ptr03_1, std::runtime_error);

    // Comparison of shared_ptr and null raw pointer of not related classes
    std::exception* pException3 = new std::exception;
    lcg::core::shared_ptr<std::exception> ptr04_1 = pException3;
    lcg::core::UdpSocket* pUdpSocket3 = NULL;
    EXPECT_NO_THROW(ptr04_1 == pUdpSocket3);

    // Comparison of null shared_ptr and raw pointer of not related classes
    lcg::core::shared_ptr<std::exception> ptr05_1;
    lcg::core::UdpSocket* pUdpSocket4 = new lcg::core::UdpSocket;
    EXPECT_THROW(ptr05_1 == pUdpSocket4, std::runtime_error);
    //delete pUdpSocket4; // Error, the pUdpSocket4 memory is freed althought the exception is thrown

    // Comparison between shared_ptr and null shared_ptr of not related classes
    std::exception* pException4 = new std::exception;
    lcg::core::shared_ptr<std::exception> ptr06_1 = pException4;
    lcg::core::shared_ptr<lcg::core::UdpSocket> ptr07_1;
    EXPECT_NO_THROW(ptr06_1 == ptr07_1);

    // Comparison between null shared_ptr and shared_ptr of not related classes
    lcg::core::shared_ptr<std::exception> ptr08_1;
    lcg::core::UdpSocket* pUdpSocket5 = new lcg::core::UdpSocket;
    lcg::core::shared_ptr<lcg::core::UdpSocket> ptr09_1 = pUdpSocket5;
    EXPECT_THROW(ptr08_1 == ptr09_1, std::runtime_error);

    // Comparison of null shared_ptr and null shared_ptr of not realated classes
    lcg::core::shared_ptr<std::exception> ptr10_1;
    lcg::core::shared_ptr<lcg::core::UdpSocket> ptr11_1;
    EXPECT_NO_THROW(ptr10_1 == ptr11_1);
}

TEST_F(shared_ptr_test, nullPointerAssignmentAndConstruction) {
    std::string nullFailureMessage =
            "The shared_ptr doesn't default to NULL while default constructing!";
    // No pointer while constructing shared_ptr
    lcg::core::shared_ptr<std::exception> ptr01_1;
    if(ptr01_1 != NULL) {
        FAIL() << nullFailureMessage;
    }

    // Null pointer construction
    lcg::core::shared_ptr<std::exception> ptr02_1(NULL);
    if(ptr02_1 != NULL) {
        FAIL() << nullFailureMessage;
    }

    // Null pointer assignment
    lcg::core::shared_ptr<std::exception> ptr03_1;
    ptr03_1 = NULL;
    if(ptr03_1 != NULL) {
        FAIL() << nullFailureMessage;
    }

    // Construction to other pointer, and then reset
    std::exception* pException1 = new std::exception;
    lcg::core::shared_ptr<std::exception> ptr04_1(pException1);
    ASSERT_EQ(ptr04_1, pException1);
    ptr04_1.reset();
    if(ptr04_1 != NULL) {
        FAIL() << nullFailureMessage;
    }

    // Assignmente to other pointer, and then reset
    std::exception* pException2 = new std::exception;
    lcg::core::shared_ptr<std::exception> ptr05_1;
    ptr05_1 = pException2;
    ASSERT_EQ(ptr05_1, pException2);
    ptr05_1.reset();
    if(ptr05_1 != NULL) {
        FAIL() << nullFailureMessage;
    }
}
