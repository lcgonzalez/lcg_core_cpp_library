// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file ThreadTests.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.7.0
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the LCG Core Cpp Library.
//
//                          License Agreement
//                     For the LCG Core Cpp Library
//
//    LCG Core Cpp Library is a lightweight open source library used to wrap
//    pthreads, Berkeley sockets, and other C libraries and functionalities,
//    into C++ objectes, over Linux, Mac OS and Windows.
//
//    LCG Core Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    LCG Core Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/


#include "lcg/core/Thread.h"

#include <stdlib.h>     // For using the function sleep
#include <climits>      // INT_MAX
#include <gtest/gtest.h>

#if(1)
class Thread_run_test : public ::testing::Test{
public:

    class TestThread : public lcg::core::Thread {
    protected:
        virtual void run() {
            testMember_ = true;
        }

    public:
        volatile bool testMember_;

        virtual void exit() {
        }
    };

    bool expectedTestMember;
    TestThread testThread;

    void SetUp() {
        testThread.testMember_ = false;
        expectedTestMember = true;

    }
    void TearDown(){
        testThread.testMember_ = false;
    }
};

TEST_F(Thread_run_test, SimpleRunTest_CallStartAndModifyVariableInsideThread)
{
    testThread.start();
    testThread.wait();
    ASSERT_EQ(testThread.testMember_, expectedTestMember);
}

TEST_F(Thread_run_test, SimpleRunTest_CallWaitWithoutStart)
{
    int successValue = 0;
    if(testThread.wait() == successValue) {
        SUCCEED();
    } else {
        FAIL();
    }
}

TEST_F(Thread_run_test, SimpleRunTest_CallStartMoreThanOnce)
{
    testThread.start();
    testThread.start();
    testThread.wait();
    testThread.testMember_ = false;
    testThread.start();
    testThread.wait();
    ASSERT_EQ(testThread.testMember_, expectedTestMember);
}

class Thread_isRunning_test : public ::testing::Test{
public:

    class TestThread : public lcg::core::Thread {
    protected:
        virtual void run() {
            while(!exitThread_) {
                testMember_ = true;
            }
        }

    public:
        volatile bool exitThread_;
        volatile bool testMember_;
        virtual void exit() {
            exitThread_ = true;
        }
    };

    TestThread testThread;
    bool expectedTestMember;

    void SetUp() {
        testThread.exitThread_ = false;
        testThread.testMember_ = false;
        expectedTestMember = true;
    }
    void TearDown(){
        testThread.exitThread_ = false;
        testThread.testMember_ = false;
    }
};

TEST_F(Thread_isRunning_test, TestIfThreadIsRunningAndExit)
{
    testThread.start();
    sleep(1);         // wait for 1 second before asserting that the thread is running
    EXPECT_EQ(testThread.isRunning(), true);
    testThread.exit();
    testThread.wait();
    EXPECT_EQ(testThread.isRunning(), false);
    ASSERT_EQ(testThread.testMember_, expectedTestMember);
}

TEST_F(Thread_isRunning_test, TestIfThreadReportsFinishedStatus)
{
    testThread.start();
    sleep(1);         // wait for 1 second before asserting that the thread hasn't finished
    EXPECT_EQ(testThread.isFinished(), false);
    testThread.exit();
    testThread.wait();
    EXPECT_EQ(testThread.isFinished(), true);
    ASSERT_EQ(testThread.testMember_, expectedTestMember);
}

TEST_F(Thread_isRunning_test, TestToRunMoreThanOneThread)
{
    TestThread testThread1;
    TestThread testThread2;

    testThread1.exitThread_ = false;
    testThread2.exitThread_ = false;
    testThread1.testMember_ = false;
    testThread2.testMember_ = false;
    testThread1.start();
    testThread2.start();
    sleep(1);         // wait for 1 second
    testThread1.exit();
    testThread1.wait();
    testThread2.exit();
    testThread2.wait();
    ASSERT_EQ(testThread1.testMember_, expectedTestMember);
    testThread1.testMember_ = false;
    ASSERT_EQ(testThread2.testMember_, expectedTestMember);
}

class Thread_mutexSync_test : public ::testing::Test{
public:

    class CounterMonitor {
    public:
        CounterMonitor():mutex_(),mCounter_(0){}
        void incAndDec(){
            mutex_.lock();  // This can be changed for a MutexLocker ml(&mutex_)
            for(int i=0; i<INT_MAX; i++) {
                mCounter_++;
            }
            for(int i=0; i<INT_MAX; i++) {
                mCounter_--;
            }
            mutex_.unlock(); // No need for this if mutex_.lock() is changed for a MutexLocker ml(&mutex_)
        }
        int getCount(){
            lcg::core::MutexLocker ml(&mutex_); // Make this thread safe
            return mCounter_;
        }
        lcg::core::Mutex* getMutex(){
            return &mutex_;
        }

    private:
        lcg::core::Mutex mutex_;
        volatile int mCounter_;
    };

    class TestThreadBase : public lcg::core::Thread {
    public:
        volatile bool exitThread_;
        volatile int testMember_;
        CounterMonitor counter;
        virtual void exit() {
            exitThread_ = true;
        }
    };

    class TestThreadWriteUnsafe : public TestThreadBase {
        virtual void run() {
            while(!exitThread_) {
                // When there is no synchronization, other threads can read
                // this variable when it's value is 1
                testMember_ = 0;
                for(int i=0; i<INT_MAX; i++) {
                    testMember_++;
                    if(exitThread_)
                        break;
                }
                for(int i=0; i<INT_MAX; i++) {
                    testMember_--;
                    if(exitThread_)
                        break;
                }
            }
        }
    };

    class TestThreadWriteSafe : public TestThreadBase {
        virtual void run() {
            while(!exitThread_) {
                // When there is no synchronization, other threads can read
                // this variable when it's value is 1
                counter.incAndDec();
            }
        }
    };

    class TestThreadReadUnsafe : public TestThreadBase {
        virtual void run() {
            while(!exitThread_) {
                testMember_=otherThread->testMember_;
            }
        }
        TestThreadBase *otherThread;

    public:
        void setOtherThread(TestThreadBase *otherThread){
            this->otherThread = otherThread;
        }
    };

    class TestThreadReadSafe : public TestThreadBase {
        virtual void run() {
            while(!exitThread_) {
                testMember_=otherThread->counter.getCount();
            }
        }
        TestThreadBase *otherThread;

    public:
        void setOtherThread(TestThreadBase *otherThread){
            this->otherThread = otherThread;
        }
    };

    TestThreadReadUnsafe testThreadReadUnsafe;
    TestThreadWriteUnsafe testThreadWriteUnsafe;
    TestThreadReadSafe testThreadReadSafe;
    TestThreadWriteSafe testThreadWriteSafe;
    int expectedTestMember;

    void SetUp() {
        testThreadReadUnsafe.exitThread_ = false;
        testThreadReadUnsafe.testMember_ = 0;
        testThreadReadUnsafe.setOtherThread(NULL);
        testThreadReadSafe.exitThread_ = false;
        testThreadReadSafe.testMember_ = 0;
        testThreadReadSafe.setOtherThread(NULL);
        testThreadWriteUnsafe.exitThread_ = false;
        testThreadWriteUnsafe.testMember_ = 0;
        testThreadWriteSafe.exitThread_ = false;
        testThreadWriteSafe.testMember_ = 0;
        expectedTestMember = 0;
    }
    void TearDown(){
        testThreadReadUnsafe.exitThread_ = false;
        testThreadReadUnsafe.testMember_ = 0;
        testThreadReadSafe.exitThread_ = false;
        testThreadReadSafe.testMember_ = 0;
        testThreadWriteUnsafe.exitThread_ = false;
        testThreadWriteUnsafe.testMember_ = 0;
        testThreadWriteSafe.exitThread_ = false;
        testThreadWriteSafe.testMember_ = 0;
    }
};

TEST_F(Thread_mutexSync_test, TestForNotInSyncDataAccess)
{
    testThreadWriteUnsafe.start();
    testThreadReadUnsafe.setOtherThread(&testThreadWriteUnsafe);
    testThreadReadUnsafe.start();

    bool success = false;
    for(int i=0; i<INT_MAX; i++) {
        if(testThreadReadUnsafe.testMember_ >= 1) {   // Succeed, accessing data is not in sync
            success = true;
            break;
        }
    }
    // Stop threads
    testThreadWriteUnsafe.exit();
    testThreadWriteUnsafe.wait();
    testThreadReadUnsafe.exit();
    testThreadReadUnsafe.wait();
    if(success) {
        SUCCEED();
    } else {
        FAIL() << "Thread Synchronization problems didn't happen in " << INT_MAX << " iterations. "
               << "Try to run the test again.";
    }
}

TEST_F(Thread_mutexSync_test, TestForDataAccessInSync)
{
    testThreadWriteSafe.start();
    testThreadReadSafe.setOtherThread(&testThreadWriteSafe);
    testThreadReadSafe.start();

    bool success = true;
    for(int i=0; i<INT_MAX; i++) {
        if(testThreadReadSafe.testMember_ >= 1) {   // Failure, accessing data is not in sync
            success = false;
            break;
        }
    }
    // Stop threads
    testThreadWriteSafe.exit();
    testThreadWriteSafe.wait();
    testThreadReadSafe.exit();
    testThreadReadSafe.wait();
    if(success) {
        SUCCEED();
    } else {
        FAIL() << "Thread Mutex is not working, access within threads is not in sync :(";
    }
}
#endif
