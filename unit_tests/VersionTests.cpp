// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file Version.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.6.1
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the LCG Core Cpp Library.
//
//                          License Agreement
//                     For the LCG Core Cpp Library
//
//    LCG Core Cpp Library is a lightweight open source library used to wrap
//    pthreads, Berkeley sockets, and other C libraries and functionalities,
//    into C++ objectes, over Linux, Mac OS and Windows.
//
//    LCG Core Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    LCG Core Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/


#include "lcg/core/Version.h"

#include <gtest/gtest.h>

typedef lcg::core::Version lcgVersion;

class Version_test : public ::testing::Test{
public:
    lcgVersion versionToCompare;
    void SetUp(){
        versionToCompare.majorVersion = 3;
        versionToCompare.minorVersion = 16;
        versionToCompare.patch = 47;
        versionToCompare.commit = 342;
    }
};

TEST_F(Version_test, compare_Equal)
{
    lcgVersion testVersion;
    testVersion.majorVersion = 3;
    testVersion.minorVersion = 16;
    testVersion.patch = 47;
    testVersion.commit = 342;

    // Both versions are the same
    ASSERT_EQ(testVersion, versionToCompare);   // 3.16.47.342 == 3.16.47.342
}

TEST_F(Version_test, compare_LessThan_And_LessThanOrEqual)
{
    lcgVersion testVersion1;
    testVersion1.majorVersion = 2;
    testVersion1.minorVersion = 16;
    testVersion1.patch = 47;
    testVersion1.commit = 342;

    // Just the major version is less than
    ASSERT_LT(testVersion1, versionToCompare);   // 2.16.47.342 < 3.16.47.342
    ASSERT_LE(testVersion1, versionToCompare);   // 2.16.47.342 <= 3.16.47.342

    lcgVersion testVersion2;
    testVersion2.majorVersion = 3;
    testVersion2.minorVersion = 15;
    testVersion2.patch = 47;
    testVersion2.commit = 342;

    // The major version is the same, but the minor version is less than
    ASSERT_LT(testVersion2, versionToCompare);   // 3.15.47.342 < 3.16.47.342
    ASSERT_LE(testVersion2, versionToCompare);   // 3.15.47.342 <= 3.16.47.342

    lcgVersion testVersion3;
    testVersion3.majorVersion = 3;
    testVersion3.minorVersion = 16;
    testVersion3.patch = 47;
    testVersion3.commit = 342;

    // Both version are the same
    ASSERT_LE(testVersion2, versionToCompare);   // 3.16.47.342 == 3.16.47.342
}

TEST_F(Version_test, compare_GreaterThan_And_GreaterThanOrEqual)
{
    lcgVersion testVersion1;
    testVersion1.majorVersion = 4;
    testVersion1.minorVersion = 16;
    testVersion1.patch = 47;
    testVersion1.commit = 342;

    // Just the major version is greater than
    ASSERT_GT(testVersion1, versionToCompare);   // 4.16.47.342 > 3.16.47.342
    ASSERT_GE(testVersion1, versionToCompare);   // 4.16.47.342 >= 3.16.47.342

    lcgVersion testVersion2;
    testVersion2.majorVersion = 3;
    testVersion2.minorVersion = 17;
    testVersion2.patch = 47;
    testVersion2.commit = 342;

    // The major version is the same, but the minor version is greater than
    ASSERT_GT(testVersion2, versionToCompare);   // 3.17.47.342 > 3.16.47.342
    ASSERT_GE(testVersion2, versionToCompare);   // 3.17.47.342 >= 3.16.47.342

    lcgVersion testVersion3;
    testVersion3.majorVersion = 3;
    testVersion3.minorVersion = 16;
    testVersion3.patch = 47;
    testVersion3.commit = 342;

    // Both version are the same
    ASSERT_GE(testVersion2, versionToCompare);   // 3.16.47.342 == 3.16.47.342
}

