// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file Version.h
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.6.1
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the LCG Core Cpp Library.
//
//                          License Agreement
//                     For the LCG Core Cpp Library
//
//    LCG Core Cpp Library is a lightweight open source library used to wrap
//    pthreads, Berkeley sockets, and other C libraries and functionalities,
//    into C++ objectes, over Linux, Mac OS and Windows.
//
//    LCG Core Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    LCG Core Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#ifndef LCG_CORE_VERSION_H
#define LCG_CORE_VERSION_H

#include "lcg/core/LCG_Core.h"  // LCG_CORE_LIB_API
#include "lcg/core/Core.h"      // lcg::core::Core

namespace lcg {
namespace core {

/**
 * @brief The Version class is a convenient class to represent the version of a software element.
 * It offers the version in the form of 4 digits, representing the following: \n
 * majorVersion.minorVersion.patch.commit
 */
class LCG_CORE_LIB_API Version : public Core {
public:
    Version(const int majorVersion=0, const int minorVersion=0,
            const int patch=0, const int commit=0);

    union{
        int fullVersion[4];
        struct{
            int majorVersion, minorVersion, patch, commit;  // This is public for convenience
        };
    };

    virtual void print() const;
    virtual bool isEqual(const Version& other) const;

    // Operators
    bool operator==(const Version& other) const;
    bool operator!=(const Version& other) const;
    bool operator<(const Version& other) const;
    bool operator<=(const Version& other) const;
    bool operator>(const Version& other) const;
    bool operator>=(const Version& other) const;

    friend std::ostream& operator<<(std::ostream& os, const Version& version);
    friend std::ostream& operator<<(std::ostream& os, const Version* version);

    static std::runtime_error throwPrintNullException();
};

}   // namespace core
}   // namespace lcg

#endif // LCG_CORE_VERSION_H
