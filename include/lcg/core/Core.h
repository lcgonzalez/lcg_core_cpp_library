// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file Core.h
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.5.1
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the LCG Core Cpp Library.
//
//                          License Agreement
//                     For the LCG Core Cpp Library
//
//    LCG Core Cpp Library is a lightweight open source library used to wrap
//    pthreads, Berkeley sockets, and other C libraries and functionalities,
//    into C++ objectes, over Linux, Mac OS and Windows.
//
//    LCG Core Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    LCG Core Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#ifndef LCG_CORE_CORE_H
#define LCG_CORE_CORE_H

#include "lcg/core/LCG_Core.h"  // LCG_CORE_LIB_API

#include <string>       // std::string
#include <ostream>      // std::ostream
#include <stdexcept>    // std::runtime_error

namespace lcg{
namespace core{

/** @class Core Core.h lcg/core/Core.h
 * @brief The Core class contains common methods that are used among all the
 * other classes implemented in this library.
 * Ideally, every new class implemented should be derived from this class,
 * in that way, every class is guaranteed to have certain functionality built in
 * that turns the class into a more practical one.
 * This functionality includes printing its content, comparison between
 * objects of the same class and string and numeric identifiers.
 *
 * Refer to the specific methods of this class to learn more about each one of
 * them.
 */
class LCG_CORE_LIB_API Core {
public:
    /**
     * @brief Constructs a Core object with name set to "no_name_set"
     * and ID set to -1.
     * @see setName() \n
     * setID()
     */
    Core();

    /**
     * @brief Destroys the Core object.
     */
    virtual ~Core();

    /**
     * @brief Prints the content of the object, in human readable format, to standard output.
     * This is a convenience method of the operator<<()
     * operator, it is equivalent to std::cout << this. \n
     * This print method, on default, prints the address of the object from which
     * is called, its name and ID.
     *
     * Reimplementation of this method should be provided by the derived classes.
     * @see operator<<(std::ostream& os, const Core& core) \n
     * operator<<(std::ostream& os, const Core* core)
     */
    virtual void print() const;

    /**
     * @brief Compares the object from which this method is called, with
     * another object of the same class.
     * This compare method, on default, compares if both objects have the same
     * name and ID.
     *
     * Reimplementation of this method should be provided by the derived classes.
     * @param other Another object of the same clase, upon which the comparision
     * will be carried out.
     * @return true if both objects are equal. \n
     * false if they are different.
     *
     * @note The concept of equality is defiend by the developer, on derived classes.
     * @see operator==(const Core& other) const \n
     * operator!=(const Core& other) const
     */
    virtual bool isEqual(const Core& other) const;

    /**
     * @brief Sets the name of the current object.
     * The name doesn't have to be unique.
     * The default name for every created object is "no_name_set".
     *
     * The ID and/or name of the object can be used to identify it among
     * other objects.
     * @param newName New name given to the current object.
     * @see setID() \n
     * getName()
     */
    virtual void setName(const std::string& newName);

    /**
     * @brief Gets the name of the current object.
     * @return If no name was previously set using setName(), the default string
     * "no_name_set" is returned. \n
     * Otherwise, the previously set name is returned.
     * @see setName()
     */
    virtual std::string getName() const;

    /**
     * @brief Sets the numeric ID of the current object.
     * The ID doesn't have to be unique.
     * The default ID for every created object is -1.
     *
     * The object's ID and/or name can be used to identify it among
     * other objects.
     * @param newID New numeric ID given to the currecnt object.
     * @see setName() \n
     * getID()
     */
    virtual void setID(const int& newID);

    /**
     * @brief Gets the numeric ID of the current object.
     * @return If no ID was previously set using setID(), the default value of
     * -1 is returned. \n
     * Otherwise, the previously set ID is returned.
     * @see setID()
     */
    virtual int getID() const;

    /**
     * @brief The operator == is a convenience representation of isEqual().
     * Please refer to isEqual() for more information.
     * @param other Another object of the same clase, upon which the comparision
     * will be carried out.
     * @return true if both objects are equal. \n
     * false if they are different.
     *
     * @note The concept of equality is defiend by the developer, on derived classes.
     * @see isEqual() \n
     * operator!=(const Core& other) const
     */
    virtual bool operator==(const Core& other) const;

    /**
     * @brief The operator != is a convenience representation of negated
     * isEqual().
     * Please refer to isEqual() for more information.
     * @param other Another object of the same clase, upon which the comparision
     * will be carried out.
     * @return ture if both objects are different. \n
     * false if they are equal.
     *
     * @note The concept of equality is defiend by the developer, on derived classes.
     * @see isEqual() \n
     * operator==(const Core& other) const
     */
    virtual bool operator!=(const Core& other) const;

    /**
     * @brief Formats the object to the specified output stream.
     * The main difference between this operator and print()
     * is that print() formats the object to the standard output
     * (std::cout), while this operator works in a more general way, allowing
     * the object to be formatted into other streams, like files, strings, etc.
     * @param os Stream to which the object will be printed (formatted).
     * Commonly std::cout, but could be any other output stream, like a
     * std::ofstring, or a std::ostreamstring.
     * @param core Object to print (format) into the output stream.
     * @return Returns the same stream where the object was printed, in order
     * to reuse it on chaining operations in the same sentence. \n
     * On error it rises an exception of type std::runtime_error.
     * @see print() \n
     * operator<<(std::ostream& os, const Core* core)
     */
    friend std::ostream& operator<<(std::ostream& os, const Core& core);

    /**
     * @brief Formats the object to the specified output stream.
     * This operator does exactly the same that operator<<(std::ostream& os, const Core& core),
     * but it receives a pointer to the object as the parameter. \n
     * If the object's address in memory needs to be printed, the default
     * implementation of print(), operator<<(std::ostream& os, const Core& core),
     * or this method will print the address.
     * @param os Stream to which the object will be printed (formatted).
     * Commonly std::cout, but could be any other output stream, like a
     * std::ofstring, or a std::ostreamstring.
     * @param core Pointer to the object to print (format) into the output stream.
     * @return Returns the same stream where the object was printed, in order
     * to reuse it on chaining operations in the same sentence. \n
     * On error it rises an exception of type std::runtime_error.
     * @see print() \n
     * operator<<(std::ostream& os, const Core& core)
     */
    friend std::ostream& operator<<(std::ostream& os, const Core* core);

protected:
    std::string name_;
    int ID_;

    /////////////////////////////// EXCEPTIONS /////////////////////////////////
    static std::runtime_error print_printNullObject_Exception();
};

std::ostream& operator<<(std::ostream& os, const Core& core);
std::ostream& operator<<(std::ostream& os, const Core* core);

} // namespace core
} // namespace lcg

#endif // LCG_CORE_CORE_H
