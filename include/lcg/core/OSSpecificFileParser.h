// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file OSSpecificFileParser.h
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.6.2
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the LCG Core Cpp Library.
//
//                          License Agreement
//                     For the LCG Core Cpp Library
//
//    LCG Core Cpp Library is a lightweight open source library used to wrap
//    pthreads, Berkeley sockets, and other C libraries and functionalities,
//    into C++ objectes, over Linux, Mac OS and Windows.
//
//    LCG Core Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    LCG Core Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#ifndef LCG_CORE_OSSPECIFICFILEPARSER_H
#define LCG_CORE_OSSPECIFICFILEPARSER_H

#include "lcg/core/LCG_Core.h"  // LCG_CORE_LIB_API
#include "lcg/core/Core.h"      // lcg::core::Core

#include <fstream>      // std::fstream
#include <iostream>     // std::fstream
#include <string>       // std::string
#include <stdexcept>    // std::runtime_error, std::domain_error
#include <sstream>      // std::ostringstream
#include <vector>       // std::vector

namespace lcg{
namespace core{

/** @class OSSpecificFileParser OSSpecificFileParser.h lcg/core/OSSpecificFileParser.h
 * @brief The OSSpecificFileParser class is useful to remove conditional code that is
 * not used by the current OS.
 * When a library, like this one, is intended for cross-platform use, and the
 * strategy used to deal with the platform specific code is to place that code
 * inside macros declared by each platform, this class becomes very handy.
 *
 * This class creates source files that only contain code specific to a platform,
 * by parsing closs-platform source files that contain plafrom specific macros
 * and getting rid of the code between those macros that are not specific to the
 * desired platform.
 */
class LCG_CORE_LIB_API OSSpecificFileParser : public Core {
    std::ifstream file_;
    std::string filePath_;
    std::stringstream ss_;  // I/O stringstream is needed to extract data to file.

    // Specific platform macro strings
    std::vector<std::string> platformsMacros_;
    std::string genericStartMacro_;
    std::string genericEndMacroStr_;
    std::string cloneStr_;

    void populateMacroStings();

public:

    /**
     * @brief Constructs a new parser, without input file path to parse or
     * platform macros.
     * The parser object should be given an input file path to parse that file,
     * using setInputFilePath().
     *
     * Before parsing, several platform macros should be added to the parser,
     * using addPlatformMacro().
     * @see setInputFilePath() \n
     * addPlatformMacro()
     */
    OSSpecificFileParser();

    /**
     * @brief Constructs a new parser, with input file path to parse, but without
     * platform macros.
     * Before parsing, several platform macros should be added to the parser,
     * using addPlatformMacro().
     * @param filePath Input file path to parse, given as a string.
     * @see addPlatformMacro()
     */
    OSSpecificFileParser(std::string filePath);

    /**
     * @brief Destroys the OSSpecificFileParser object.
     **/
    ~OSSpecificFileParser();

    /**
     * @brief Tells the parser which new file to parse.
     * A path (relative or absolute) leading to a source file to parse, can
     * be given to the parser using this method.
     *
     * Before parsing, several platform macros should be added to the parser,
     * using addPlatformMacro().
     * @param newFilePath Input file path to parse, given as a string.
     * @see addPlatformMacro()
     */
    void setInputFilePath(std::string newFilePath);

    /**
     * @brief Adds new platform macros for the parse to process the source files.
     * All knwon macros of the form "#ifdef __PLATFORM" that the source files use
     * should be given to the parser before calling parse().
     *
     * Notice: Just the unique part "__PLATFORM" should be added through this metod,
     * the part "#ifdef" should and must be avoided, because it is not unique.
     * @param platformMacro Unique string defining one of the platform macros
     * that the source file uses. The string should be of the form: "__PLATFORM".
     * @see parse()
     */
    void addPlatformMacro(std::string platformMacro);   // "__LINUX", "__WINDOWS", etc.

    /**
     * @brief Clears all the platform macros perviously added.
     * @see addPlatformMacro()
     */
    void clearPlatformMacros();

    /**
     * @brief Parses the input file, using the previously added macros, according to the given macro.
     * The input file needs to be previously set using setInputFilePath(),
     * and the known platform macros should also be added before calling
     * this method, using addPlatformMacro().
     * The current platform macro should be given as the argument to this method.
     * @param platformMacroString String representing the current platform macro, it should
     * be one of the macros previously added using addPlatformMacro().
     * @return On success 0. \n
     * On failure, an exception of type std::domain_error is rised if the
     * platformMacroString parameter contains an unknown platform macro. \n
     * Otherwise, it rises an exception of type std::runtime_error.
     * @see setInputFilePath() \n
     * addPlatformMacro()
     */
    int parse(std::string platformMacroString);

    /**
     * @brief Writes the previously parsed file to the specified path.
     * @param writingPath Path where the parsed file will be written.
     * @return On success 0. \n
     * On failure, an exception of type std::runtime_error is rised.
     */
    int writeToPath(std::string writingPath);

    /**
     * @brief Compares two text files to determine if they are equal or not.
     * Both files should exist, otherwise, an std::runtime_error exception
     * is rised.
     *
     * @param filePath_A Path of the first file to compare.
     * @param filePath_B Path of the second file to compare.
     * @return 0 if both files are identical. \n
     * -1 if there is a difference among the files.
     *
     * If there is an error, like any of the files doesn't exist, an
     * std::runtime_error exception is rised.
     */
    static int compareTextFiles(std::string filePath_A, std::string filePath_B);

private:
    //////////////////////////// EXCEPTIONS ///////////////////////////////////
    static std::runtime_error parseFile_FileIsNotOpen_Exception(int errorCode, std::string filePath);
    std::runtime_error writeToPath_FileNotOpened_Exception(int errorCode, std::string filePath);
    std::runtime_error writeToPath_writingNotParsedFile_Exception();
    std::domain_error parseFile_UnknownPlatformMacroString_Exception(std::string unknownPlatformMacroString);
};

}   // namespace core
}   // namespace lcg

#endif // LCG_CORE_OSSPECIFICFILEPARSER_H
