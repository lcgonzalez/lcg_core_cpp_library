// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file Thread.h
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.6.2
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the LCG Core Cpp Library.
//
//                          License Agreement
//                     For the LCG Core Cpp Library
//
//    LCG Core Cpp Library is a lightweight open source library used to wrap
//    pthreads, Berkeley sockets, and other C libraries and functionalities,
//    into C++ objectes, over Linux, Mac OS and Windows.
//
//    LCG Core Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    LCG Core Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#ifndef LCG_CORE_THREAD_H
#define LCG_CORE_THREAD_H

#include "lcg/core/Core.h"  // lcg::Core

#include <pthread.h>        // pthread_mutex_t, pthread_t
#include <stdexcept>        // std::runtime_error

namespace lcg{
namespace core{

namespace C_ {
extern "C"
void *startThread(void* arg);
}

/** @class Thread Thread.h lcg/core/Thread.h
 * @brief The Thread class provides the basis to derivate a class to define the thread code.
 * When needing to run code in another thread, this class should be derived in
 * a subclass, which pure virtual methods Thread::run() and Thread::exit() should
 * be implemented, in order to add the code that the new thread will execute,
 * and code to signal a safe exit of this thread.
 * @see Thread::run() \n
 * Thread::exit()
 */
class LCG_CORE_LIB_API Thread {
public:
    /**
     * @brief Constructs a new Thread object, ready to be started by start().
     * The Thread should be started by calling start(), and stoped by exit().
     * @see start() \n
     * exit() \n
     * wait()
     */
    Thread();

    /**
     * @brief Destroys the Thread object and nothing else, however it should call exit().
     * Because exit() is pure virtual, it can't be called from this destructor
     * until it's implemented on a derived class. \n
     * @note The destructor of the derived class
     * should explicitly call exit(), which should be implemented in that class.
     * @see exit()
     **/
    virtual ~Thread();

    /**
     * @brief Starts the new thread defined by run().
     * This method should be called after a new thread has been derived from
     * this class and the run() and exit() methods have been implemented. \n
     * The new thread will start at run(), and the contents of this
     * method should be given by the implementation on the deirved class.
     *
     * @note The way of exiting or "stopping" the new thread should be given
     * by implementing exit() on the derived class (by means of changing
     * a centinel or flag variable inside the thread, for example).
     * @return 0 on success. \n
     * On failure it rises an exception of type std::runtime_error.
     * @see exit() \n
     * run()
     */
    int start();

    /**
     * @brief Tells whether or not the thread is done.
     * Tells if the code inside run() (the new thread) has finished its execution.
     * @return True if the thread has finished its execution. \n
     * False if it's still running or the thread is in an uninitialized state.
     * @see start() \n
     * run() \n
     * isRunning \n
     */
    bool isFinished();

    /**
     * @brief Tells whether or not the thread is still running.
     * Tells if the code inside run() (the new thread) is still being executed
     * by the OS.
     * @return True if the thread is still running. \n
     * False otherwise.
     * @see start() \n
     * run() \n
     * isFinished()
     */
    bool isRunning();

    /**
     * @brief Makes the calling thread wait for the thread to finish its execution (joins).
     * When this method is invoked, the thread that called it will be blocked unitl
     * this method returns. \n
     * This method will return when the object's thread finishes its execution,
     * or under an error waiting to the thread to finish its execution.
     * @return 0 on success. \n
     * On failure it rises an exception of type std::runtime_error.
     * @see start()
     */
    int wait();

    /**
     * @brief Signals the thread to safely finish its execution.
     * This method should be implemtend in the derived Thread class.
     * It should be implemented in a way that changes the value of a flag
     * variable that will be used inside run() to signal the thread
     * to safely finish its execution and realese its resources.
     * @see run() \n
     * start()
     */
    virtual void exit() = 0;

protected:
    /**
     * @brief Describes the state of the Thread.
     */
    enum ThreadState {
        UnInitialized = 0x0, /**< The thread has been created, but is waiting to start */
        Running = 0x1,       /**< The thread is running (inside run()). */
        Finished = 0x2       /**< The thread has finished its execution. */
    };
    ThreadState threadState_;

    /**
     * @brief Contains the code that will be executed in the new thread.
     * This method should not be directly called, insted the public method
     * start() should be called to start the execution of the new thred.
     *
     * @note This method should be implemented in the derived class,
     * as well as exit().
     * A flag variable should be included inside this method logic
     * that would signal to safely finish the thread execution and release its
     * resources, this flag variable should be controlled from inside
     * exit() on the derived class.
     * @see exit() \n
     * start() \n
     * wait()
     */
    virtual void run() = 0;

private:
    pthread_t threadID_;

    friend void *C_::startThread(void* arg);

    /////////////////////////////// EXCEPTIONS /////////////////////////////////
    std::runtime_error start_pthreadCreateError_Exception(int errorCode);
    std::runtime_error wait_pthreadJoinError_Exception(int errorCode);
    std::runtime_error start_unknownThreadState_Exception();
};

// TODO: Add the Windows thread implementation
/** @class Mutex Thread.h lcg/core/Thread.h
 * @brief The Mutex class creates a mutex that can be used to synchronize access to threads.
 * Whenever shared resources are used between threads, a safe technique to use
 * the resources is needed in order to guarantee safe resource access. One of these
 * techniques is the use of mutexes.
 * @see Mutex::lock() \n
 * Mutex::unlock() \n
 * MutexLocker
 */
class LCG_CORE_LIB_API Mutex : public Core {
public:
    /**
     * @brief Constructs a new Mutex object to be used by threads.
     * It rises an exception of type std::runtime_error if the Mutex couldn't be crated.
     */
    Mutex();

    /**
     * @brief Unlocks and destroys the Mutex object.
     * Internally, the destructor calls unlock.
     * @see unlock()
     */
    virtual ~Mutex();

    /**
     * @brief Locks the Mutex in the current thread.
     * When not locked, or unlocked by the thread that locked the mutex, this
     * method will return immediately, effectively locking the mutex for the
     * current thread that called this method. \n
     * When locked, this method will not return on other threads until the
     * thread that locked the mutex unlocks it using the unlock method.
     *
     * @note Attempting to relock, or lock a Mutex when already locked by that thread,
     * will result in a DEADLOCK.
     * @return 0 on success. \n
     * On failure it rises an exception of type std::runtime_error.
     * @see unlock()
     */
    int lock();

    /**
     * @brief Unlocks the Mutex in the current thread.
     * If the Mutex is not locked, it will return without doing anything. \n
     * If the Mutex is locked by the same thread that calls this method, it
     * will unlock the Mutex. \n
     * If the Mutex is locked by other thread, it causes undefined behaviour.
     *
     * @note This method should be called after a call to lock() on the same
     * thread, otherwise, undefined behaviour will result.
     * @return 0 on success. \n
     * On failure it rises an exception of type std::runtime_error.
     * @see lock()
     */
    int unlock();
private:
    // TODO: This constructors are private until copy issues with threads
    // are solved. Meanwhile, pass Mutex with pointers.
    Mutex (const Mutex& other); // Copy constuctor
    Mutex& operator= (const Mutex& other); // // Copy assignment operator

    pthread_mutex_t* lock_;
    bool locked_;
    //unsigned int* numberOfMutexes; // Number of copied instances of the same Mutex
    friend class MutexLocker;

    /////////////////////////////// EXCEPTIONS /////////////////////////////////
    std::runtime_error Mutex_pthreadMutexInitError_Exception(int errorCode);
    std::runtime_error lock_pthreadMutexLockError_Exception(int errorCode);
    std::runtime_error unlock_pthreadMutexUnlock_Exception(int errorCode);
};

/** @class MutexLocker Thread.h lcg/core/Thread.h
 * @brief The MutexLocker class is a convenience class to handle the Mutex locking and unlocking.
 * Whenever an object of this class is created, with a pointer to a Mutex as its
 * argument, the Mutex will be locked, until a call to unlock is made, or the
 * object goes out of scope. \n
 * If the object goes out of scope, the destructor will call Mutex::unlock(), effectively
 * and automatically unlocking the Mutex.
 * @see Mutex
 */
class LCG_CORE_LIB_API MutexLocker {
public:
    /**
     * @brief Constructs a MutexLocker object, locking the Mutex given.
     * The Mutex will be locked by the calling thread until explicity unlocked
     * by unlock() or the MutexLocker object goes out of scope.
     *
     * It is equivalent to calling Mutex::lock() over the Mutex object.
     *
     * @param mutex Pointer to a Mutex object that will be locked.
     * @see Mutex \n
     * ~MutexLocker() \n
     * unlock()
     */
    explicit MutexLocker(Mutex *mutex);

    /**
     * @brief Unlocks the Mutex and destroys the MutexLocker object.
     * Internally, it calls unlock().
     * @see Mutex \n
     * unlock()
     **/
    ~MutexLocker();

    /**
     * @brief Gets the Mutex object.
     * @return A pointer to the Mutex object that is being locked and
     * unlocked by this class.
     */
    Mutex* getMutex();

    /**
     * @brief Attempts to lock the Mutex.
     * If the Mutex is not locked, it will lock it. \n
     * If the Mutex is locked by other thread, the method will block until the
     * other thread unlocks it. \n
     * If the Mutex is locked by the same thread that calls this method, it
     * will enter in a DEADLOCK.
     *
     * @note It is equivalent to calling Mutex::lock(). \n
     * Attempting to relock, or lock a Mutex when already locked by that thread,
     * will result in a DEADLOCK.
     *
     * @return 0 on success. \n
     * On failure it rises an exception of type std::runtime_error.
     * @see Mutex \n
     * Mutex::lock() \n
     * unlock()
     */
    int relock();

    /**
     * @brief Unlocks the Mutex.
     * If the Mutex is not locked, it will return without doing anything. \n
     * If the Mutex is locked by the same thread that calls this method, it
     * will unlock the Mutex. \n
     * If the mutex is locked by other thread, it causes undefined behaviour.
     *
     * @note It is equivalent to calling Mutex::unlock(). \n
     * This method should be called after a call to lock() on the same thread,
     * otherwise, undefined behaviour will result.
     *
     * @return 0 on success. \n
     * On failure it rises an exception of type std::runtime_error.
     * @see Mutex \n
     * Mutex::lock() \n
     * unlock()
     */
    int unlock();

private:
    MutexLocker();  // A MutexLocker without a mutex doesn't make sense
    Mutex *mutex_;
};

} // namespace core
} // namespace lcg

#endif // LCG_CORE_THREAD_H
