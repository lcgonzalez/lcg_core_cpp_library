// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file Number.h
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.5.1
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the LCG Core Cpp Library.
//
//                          License Agreement
//                     For the LCG Core Cpp Library
//
//    LCG Core Cpp Library is a lightweight open source library used to wrap
//    pthreads, Berkeley sockets, and other C libraries and functionalities,
//    into C++ objectes, over Linux, Mac OS and Windows.
//
//    LCG Core Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    LCG Core Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#ifndef LCG_CORE_NUMBER_H
#define LCG_CORE_NUMBER_H

#include "lcg/core/LCG_Core.h"  // LCG_CORE_LIB_API

#include <string>   // std::string

namespace lcg{
namespace core{

/** @class Number Number.h lcg/core/Number.h
 * @brief The Number class provides several helper static methods related to numbers.
 */
class LCG_CORE_LIB_API Number {
public:
    /**
     * @brief Formats an int into its string representation.
     * @param number Number to format into string.
     * @return std::string representing the formatted number.
     */
    static std::string intToString(int number);

private:
    Number();
};

template<class T>
/** @class numeric_limits Number.h lcg/core/Number.h
 * @brief The numeric_limits class is a template useful when formatting elemental types to string.
 * The template is useful when the total current platform precision is going to be
 * formatted into a string representation, the max_digits10 constant holds the
 * maximum number of digits that should be taken into account for a given number, in order
 * to format the full precision of that number.
 * This constant is already present on C++11, so there is no need to use it if
 * using a C++11 compiler.
 * @see numeric_limits::max_digits10.
 */
class numeric_limits {
public:
    /**
     * @brief Holds the maximum number of digits that should be
     * taken into account for a given number, in order to format the full
     * precision of that number.
     * This constant is already present on C++11, so there is no need to use it if
     * using a C++11 compiler.
     */
    static const int max_digits10;
};

template<class T>
const int numeric_limits<T>::max_digits10 = 0; // Not specialized
// Specializations declarations
template<>
const int numeric_limits<bool>::max_digits10;
template<>
const int numeric_limits<char>::max_digits10;
template<>
const int numeric_limits<signed char>::max_digits10;
template<>
const int numeric_limits<unsigned char>::max_digits10;
template<>
const int numeric_limits<wchar_t>::max_digits10;
template<>
const int numeric_limits<short>::max_digits10;
template<>
const int numeric_limits<unsigned short>::max_digits10;
template<>
const int numeric_limits<int>::max_digits10;
template<>
const int numeric_limits<unsigned int>::max_digits10;
template<>
const int numeric_limits<long>::max_digits10;
template<>
const int numeric_limits<unsigned long>::max_digits10;
template<>
const int numeric_limits<long long>::max_digits10;
template<>
const int numeric_limits<unsigned long long>::max_digits10;
template<>
const int numeric_limits<float>::max_digits10;
template<>
const int numeric_limits<double>::max_digits10;
template<>
const int numeric_limits<long double>::max_digits10;

} // namespace core
} // namespace lcg

#endif // LCG_CORE_NUMBER_H
