// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file memory.h
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.7.0
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the LCG Core Cpp Library.
//
//                          License Agreement
//                     For the LCG Core Cpp Library
//
//    LCG Core Cpp Library is a lightweight open source library used to wrap
//    pthreads, Berkeley sockets, and other C libraries and functionalities,
//    into C++ objectes, over Linux, Mac OS and Windows.
//
//    LCG Core Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    LCG Core Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#ifndef LCG_CORE_MEMORY_H
#define LCG_CORE_MEMORY_H

#include "lcg/core/LCG_Core.h"  // LCG_CORE_LIB_API

#include <map>          // std::map
#include <stdexcept>    // std::runtime_error

namespace lcg {
namespace core {

/** @class ___shared_ptr memory.h lcg/core/memory.h
 * @brief The ___shared_ptr class is the real smart pointer class to manage and garbage collect raw pointers.
 *
 * This is the real implementation of shared_ptr, while the later class is just a wraper
 * to this class.
 * The reason for having this wraper is because this class, the real one, has too many
 * public methods and fields, that should be private, but due to the templated covariance some methods, operators,
 * and constructors, manage other objects as if they belonged to a different class that is not
 * related to this object, thus the access modifiers matters on those objects.
 *
 * So, in order to have a cleaner, encapsulated class, the wraper class, shared_ptr, was created.
 */
template<class T>
class LCG_CORE_LIB_API ___shared_ptr {
private:
    T* rawPtr;

    std::runtime_error shared_ptrNotSameClassFamilyPointerException() const {
        return std::runtime_error("Attemtping to assign (cast) pointers to objects that are not related or in the same class family.");
    }

    std::runtime_error dereferenceNullPointerException() const {
        return std::runtime_error("Attempting to dereference a NULL shared_ptr");
    }

    template<class U>
    void updateCovariancePointer(const ___shared_ptr<U>& other) {
        ___shared_ptr<U>& nonConstOther = const_cast<___shared_ptr<U>&>(other);
        rawPtr = dynamic_cast<T*>(nonConstOther.get());
        if(rawPtr == NULL) {    // Error, other doesn't belong to the same class family as *this
            // Rise exception
            throw shared_ptrNotSameClassFamilyPointerException();
        }
        virtualReferences = nonConstOther.virtualReferences;
        std::map<void*, long long>* otherReferencesCounter = reinterpret_cast<std::map<void*, long long>*>(&(nonConstOther.referencesCounterMap));
        virtualReferences[otherReferencesCounter] = rawPtr;
    }

    void updatePointer(const ___shared_ptr& other) {
        rawPtr = other.rawPtr;
        virtualReferences = other.virtualReferences;
    }

    void increaseReferencesToPointer() {
        if( rawPtr != NULL) {
            referencesCounterMap[rawPtr]++;
            for(std::map<std::map<void*, long long>*, void*>::iterator it = virtualReferences.begin();
                it != virtualReferences.end(); it++) {
                std::map<void*, long long>* virtualReferencesCounterMap = it->first;

                if( reinterpret_cast<void*>(virtualReferencesCounterMap)
                    != reinterpret_cast<void*>(&referencesCounterMap) ) {    // Avoid double counting references (avoid self reference)
                    void* virtualRawPtr = it->second;
                    if( virtualReferencesCounterMap != NULL && virtualRawPtr != NULL) {
                        std::map<void*, long long>& tmpMap = *virtualReferencesCounterMap;
                        tmpMap[virtualRawPtr]++;
                    }
                }
            }
        }
    }

    void decreaseReferencesToPointer() {
        long long nVirtualPointers = 0;

        // Check if there are other pointers of the same family (virtual) alive
        for(std::map<std::map<void*, long long>*, void*>::iterator it = virtualReferences.begin();
            it != virtualReferences.end(); it++) {
            std::map<void*, long long>* virtualReferencesCounterMap = it->first;

            if( reinterpret_cast<void*>(virtualReferencesCounterMap)    // Avoid double counting references (avoid self reference)
                    != reinterpret_cast<void*>(&referencesCounterMap)) {
                void* virtualRawPtr = it->second;
                if( virtualReferencesCounterMap != NULL && virtualRawPtr != NULL) {
                    std::map<void*, long long>& tmpVirtualReferencesMap = *virtualReferencesCounterMap;
                    if( tmpVirtualReferencesMap[virtualRawPtr] > 0) {
                        tmpVirtualReferencesMap[virtualRawPtr]--;
                        nVirtualPointers += tmpVirtualReferencesMap[virtualRawPtr];
                    }
                }
            }
        }

        if( rawPtr != NULL && referencesCounterMap[rawPtr] > 0 ) {    // Only if there are references to this pointer
            referencesCounterMap[rawPtr]--;
            nVirtualPointers += referencesCounterMap[rawPtr];
        }

        // Delete pointer only if there are no references in this object or in the other virtual ones
        if(nVirtualPointers <= 0 && rawPtr != NULL) {
            delete rawPtr;
            rawPtr = NULL;

            // Invalidate all reterences in other virtual objects
            for(std::map<std::map<void*, long long>*, void*>::iterator it = virtualReferences.begin();
                it != virtualReferences.end(); it++) {
                it->second = NULL;
            }
        }
    }

public:
    // These are public because when usign the templated covariance methods, operators
    // and constructors, the other objects belong to a different class that is not
    // related to this object, thus the access modifiers matters on those objects.
    static std::map<T*, long long> referencesCounterMap;
    std::map<std::map<void*, long long>* /*virtualReferencesMap*/, void* /*virtualRawPtr*/> virtualReferences;

    ___shared_ptr() :          // Default constructor
        rawPtr(NULL)
    {}

    ___shared_ptr(const T* rawPtr) : // Initialization constructor
        rawPtr(const_cast<T*>(rawPtr))
    {
        if(rawPtr != NULL) {
            increaseReferencesToPointer();
        }
    }

    template<class U>
    ___shared_ptr(const U* otherRawPtr) {  // Template initialization constructor (for template covariance) or different raw type
        if(otherRawPtr != NULL) {
            U* nonConstOtherRawPtr = const_cast<U*>(otherRawPtr);
            ___shared_ptr<U> other(nonConstOtherRawPtr);   // Double increasing references to other
            updateCovariancePointer(other);
            increaseReferencesToPointer();      // Double increasing references to other
        } else {
            rawPtr = NULL;  // NULL pointer assignment
        }
    }

    ___shared_ptr(const ___shared_ptr& other) : // Copy constructor (same type)
        rawPtr(other.rawPtr)
    {
        virtualReferences = other.virtualReferences;
        increaseReferencesToPointer();
    }

    template<class U>
    ___shared_ptr(const ___shared_ptr<U>& other) { // Template copy constructor (for template covariance) or different type
        if(other.get() != NULL) {
            updateCovariancePointer(other);
            increaseReferencesToPointer();
        } else {
            rawPtr = NULL;  // NULL pointer assignment
        }
    }

    ~___shared_ptr() {
        reset();
    }

    T* get() const {
        return rawPtr;
    }

    void reset() {
        if( rawPtr != NULL) {
            decreaseReferencesToPointer();
            virtualReferences.clear();
            rawPtr = NULL;
        }
    }

    ////////////////// OPERATORS ///////////////////////////////////

    ___shared_ptr& operator=(const ___shared_ptr& other) {   // Assignment operator
        if(rawPtr != other.rawPtr) {    // Protect from self assignment
            reset();  // Disown reference to the previous pointer
            if(other.get() != NULL) {
                updatePointer(other);
                increaseReferencesToPointer();
            }
        }
        return *this;
    }

    template<class U>
    ___shared_ptr& operator=(const ___shared_ptr<U>& other) {   // Template assignment operator (for template covariance) or different type
        reset();  // Disown reference to the previous pointer
        if( other.get() != NULL ) {
            updateCovariancePointer(other);
            increaseReferencesToPointer();
        }
        return *this;
    }

    T& operator*() const {
        if(rawPtr == NULL) {
            throw dereferenceNullPointerException();
        }
        return *rawPtr;
    }

    T* operator->() const {
        if(rawPtr == NULL) {
            throw dereferenceNullPointerException();
        }
        return rawPtr;
    }

    bool operator==(const ___shared_ptr<T>& other) const {
        return rawPtr == other.rawPtr;
    }

    bool operator!=(const ___shared_ptr& other) const {
        return rawPtr != other.rawPtr;
    }

    bool operator<(const ___shared_ptr& other) const {
        return rawPtr < other.rawPtr;
    }

    bool operator<=(const ___shared_ptr& other) const {
        return rawPtr <= other.rawPtr;
    }

    bool operator>(const ___shared_ptr& other) const {
        return rawPtr > other.rawPtr;
    }

    bool operator>=(const ___shared_ptr& other) const {
        return rawPtr >= other.rawPtr;
    }
};

template<class T>
std::map<T*, long long> ___shared_ptr<T>::referencesCounterMap;

/** @class shared_ptr memory.h lcg/core/memory.h
 * @brief The shared_ptr class provides a smart pointer to manage and garbage collect raw pointers.
 *
 * This kind of smart pointer will keep track of references even when they were
 * given by raw pointers, in other words, if two instances of shared_ptr are given
 * the same raw pointer to manage, they will not call delete twice, solving this
 * problem:
 * https://yatb.giacomodrago.com/en/post/11/cpp11-smart-pointers-need-careful-programmers.html
 */
template<class T>
class LCG_CORE_LIB_API shared_ptr {
public:
    // This is public because when usign the templated covariance methods, operators
    // and constructors, the other objects belong to a different class that is not
    // related to this object, thus the access modifiers matters on those objects.
    ___shared_ptr<T> ___shared_ptr_;

    /**
     * @brief shared_ptr Default constructor that initializes the underlying raw pointer to NULL.
     */
    shared_ptr() :  // Default constructor
        ___shared_ptr_()
    {}

    /**
     * @brief shared_ptr Copy constructor (raw type).
     * Copy other raw pointer of the same type into this one, effectively incresing the number of
     * reference of the other raw ponter.
     * @param rawPtr Raw pointer of the same type than that of this smart pointer.
     */
    shared_ptr(const T* rawPtr) : // Initialization constructor
        ___shared_ptr_(rawPtr)
    {}

    /** @brief Template initialization constructor (for template covariance) or different raw type.
     * Copy other raw ponter of different type into this one, effectively incresing the number of
     * reference of the other raw ponter.
     *
     * If the otherRawPtr doesn't belong to the same class family (dynamic_cast fails),
     * then this constructor will throw a std::runtime_error exception, and a delete
     * or "reset" will be attempted in the other raw type pointer "otherRawPtr".
     * @param otherRawPtr Raw pointer of different type than that of this smart pointer.
     */
    template<class U>
    shared_ptr(const U* otherRawPtr) :
        ___shared_ptr_(otherRawPtr)
    {}

    /**
     * @brief shared_ptr Copy constructor (same type).
     * Copy other shared_ptr object, of the same type, into this one, effectively incresing the number of
     * reference of the other object.
     * @param other shared_ptr object of the same type than this smart pointer.
     */
    shared_ptr(const shared_ptr& other) :
        ___shared_ptr_(other.___shared_ptr_)
    {}

    /** Template copy constructor (for template covariance) or different type.
     * Copy other shared_ptr object, of different type, into this one, effectively incresing the number of
     * reference of the other object.
     *
     * If the other object doesn't belong to the same class family (dynamic_cast fails),
     * then this constructor will throw a std::runtime_error exception. No further delete
     * or "reset" will be attempted in the other shared_ptr "other".
     * @param other shared_ptr object of different type than this smart pointer.
     */
    template<class U>
    shared_ptr(const shared_ptr<U>& other) :
        ___shared_ptr_(other.___shared_ptr_)
    {}

    /**
     * @brief get will get the actual real raw pointer that is being managed.
     * @return The raw pointer that this shared_ptr is managing.
     */
    T* get() const {
        return ___shared_ptr_.rawPtr;
    }

    /**
     * @brief reset diswons the raw pointer being managed.
     * This method effectively reduces, by one, the number of references that the underlying
     * raw ponter possess. If this number reaches zero, then the managed raw pointer
     * is deleted, and the resources are returned to memory.
     */
    void reset() {
        ___shared_ptr_.reset();
    }

    ////////////////// OPERATORS ///////////////////////////////////
    /**
     * @brief operator = Copy other shared_ptr object of the same type into this one,
     * effectively incresing the number of
     * references of the other object, and decreasing the number of references of this
     * one, potentially deleting the underlying raw pointer of this object if no more
     * references point to it.
     * @param other
     * @return
     */
    shared_ptr& operator=(const shared_ptr& other) {   // Assignment operator
        ___shared_ptr_ = other.___shared_ptr_;
        return *this;
    }

    /** // Template assignment operator (for template covariance) or different type.
     * Copy other shared_ptr object of different type into this one, effectively incresing the number of
     * references of the other object, and decreasing the number of references of this
     * one, potentially deleting the underlying raw pointer of this object if no more
     * references point to it.
     *
     * If the other object doesn't belong to the same class family (dynamic_cast fails),
     * then this constructor will throw a std::runtime_error exception. No further delete
     * or "reset" will be attempted in the other shared_ptr "other".
     */
    template<class U>
    shared_ptr& operator=(const shared_ptr<U>& other) {
        ___shared_ptr_ = other.___shared_ptr_;
        return *this;
    }

    /**
     * @brief operator * Dereferences the smart pointer, just like a raw pointer.
     * @return A reference to the actual object being managed in memory.
     */
    T& operator*() const {      // Dereference operator
        return *___shared_ptr_;
    }

    /**
     * @brief operator -> Access the memebers of the object being managed, just like a raw pointer.
     * @return A pointer to the actual object being managed in memory.
     */
    T* operator->() const {     // Arrow operator
        return ___shared_ptr_.operator->();
    }

    /**
     * @brief operator == checks the actual pointers, not the contents of them.
     * Due to implicit constructors initialization, if an object of another
     * class family is used with this operator, the templated constructior will
     * be called to construct an object of the same class as this one. \n
     * In that process, if the other parameter is a raw pointer or object that
     * doesn't belong to the same class family (dynamic_cast fails), then that
     * constructor, and therefore, this operator, will throw a std::runtime_error
     * exception. An attempt to delete or "reset" the other raw pointer, but not
     * the other shared_ptr object, will be carried out.
     * @param other Other shared_ptr object, it can be from other class (template
     * covariance), or a raw pointer, that will be turned into a shared_ptr automatically,
     * thanks to implicit constructors.
     * @return True if both pointers are the same (the actual address, not the contents). \n
     * False otherwise.
     */
    bool operator==(const shared_ptr<T>& other) const {     // Equal to operator
        return ___shared_ptr_ == other.___shared_ptr_;
    }

    /**
     * @brief operator != checks the actual pointers, not the contents of them.
     * Due to implicit constructors initialization, if an object of another
     * class family is used with this operator, the templated constructior will
     * be called to construct an object of the same class as this one. \n
     * In that process, if the other parameter is a raw pointer or object that
     * doesn't belong to the same class family (dynamic_cast fails), then that
     * constructor, and therefore, this operator, will throw a std::runtime_error
     * exception. An attempt to delete or "reset" the other raw pointer, but not
     * the other shared_ptr object, will be carried out.
     * @param other Other shared_ptr object, it can be from other class (template
     * covariance), or a raw pointer, that will be turned into a shared_ptr automatically,
     * thanks to implicit constructors.
     * @return True if both pointers are the same (the actual address, not the contents). \n
     * False otherwise.
     */
    bool operator!=(const shared_ptr& other) const {     // Not equal to operator
        return ___shared_ptr_ != other.___shared_ptr_;
    }

    /**
     * @brief operator < checks the actual pointers, not the contents of them.
     * Due to implicit constructors initialization, if an object of another
     * class family is used with this operator, the templated constructior will
     * be called to construct an object of the same class as this one. \n
     * In that process, if the other parameter is a raw pointer or object that
     * doesn't belong to the same class family (dynamic_cast fails), then that
     * constructor, and therefore, this operator, will throw a std::runtime_error
     * exception. An attempt to delete or "reset" the other raw pointer, but not
     * the other shared_ptr object, will be carried out.
     * @param other Other shared_ptr object, it can be from other class (template
     * covariance), or a raw pointer, that will be turned into a shared_ptr automatically,
     * thanks to implicit constructors.
     * @return True if both pointers are the same (the actual address, not the contents). \n
     * False otherwise.
     */
    bool operator<(const shared_ptr& other) const {  // Less than operator
        return ___shared_ptr_ < other.___shared_ptr_;
    }

    /**
     * @brief operator <= checks the actual pointers, not the contents of them.
     * Due to implicit constructors initialization, if an object of another
     * class family is used with this operator, the templated constructior will
     * be called to construct an object of the same class as this one. \n
     * In that process, if the other parameter is a raw pointer or object that
     * doesn't belong to the same class family (dynamic_cast fails), then that
     * constructor, and therefore, this operator, will throw a std::runtime_error
     * exception. An attempt to delete or "reset" the other raw pointer, but not
     * the other shared_ptr object, will be carried out.
     * @param other Other shared_ptr object, it can be from other class (template
     * covariance), or a raw pointer, that will be turned into a shared_ptr automatically,
     * thanks to implicit constructors.
     * @return True if both pointers are the same (the actual address, not the contents). \n
     * False otherwise.
     */
    bool operator<=(const shared_ptr& other) const {    // Less than or equal to operator
        return ___shared_ptr_ <= other.___shared_ptr_;
    }

    /**
     * @brief operator > checks the actual pointers, not the contents of them.
     * Due to implicit constructors initialization, if an object of another
     * class family is used with this operator, the templated constructior will
     * be called to construct an object of the same class as this one. \n
     * In that process, if the other parameter is a raw pointer or object that
     * doesn't belong to the same class family (dynamic_cast fails), then that
     * constructor, and therefore, this operator, will throw a std::runtime_error
     * exception. An attempt to delete or "reset" the other raw pointer, but not
     * the other shared_ptr object, will be carried out.
     * @param other Other shared_ptr object, it can be from other class (template
     * covariance), or a raw pointer, that will be turned into a shared_ptr automatically,
     * thanks to implicit constructors.
     * @return True if both pointers are the same (the actual address, not the contents). \n
     * False otherwise.
     */
    bool operator>(const shared_ptr& other) const {     // Greater than operator
        return ___shared_ptr_ > other.___shared_ptr_;
    }

    /**
     * @brief operator >= checks the actual pointers, not the contents of them.
     * Due to implicit constructors initialization, if an object of another
     * class family is used with this operator, the templated constructior will
     * be called to construct an object of the same class as this one. \n
     * In that process, if the other parameter is a raw pointer or object that
     * doesn't belong to the same class family (dynamic_cast fails), then that
     * constructor, and therefore, this operator, will throw a std::runtime_error
     * exception. An attempt to delete or "reset" the other raw pointer, but not
     * the other shared_ptr object, will be carried out.
     * @param other Other shared_ptr object, it can be from other class (template
     * covariance), or a raw pointer, that will be turned into a shared_ptr automatically,
     * thanks to implicit constructors.
     * @return True if both pointers are the same (the actual address, not the contents). \n
     * False otherwise.
     */
    bool operator>=(const shared_ptr& other) const {    // Greater than or equal to operator
        return ___shared_ptr_ >= other.___shared_ptr_;
    }

};

}   // namespace core
}   // namespace lcg

#endif // LCG_CORE_MEMORY_H
