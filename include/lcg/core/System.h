// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file System.h
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.5.1
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the LCG Core Cpp Library.
//
//                          License Agreement
//                     For the LCG Core Cpp Library
//
//    LCG Core Cpp Library is a lightweight open source library used to wrap
//    pthreads, Berkeley sockets, and other C libraries and functionalities,
//    into C++ objectes, over Linux, Mac OS and Windows.
//
//    LCG Core Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    LCG Core Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#ifndef LCG_CORE_SYSTEM_H
#define LCG_CORE_SYSTEM_H

#include "lcg/core/LCG_Core.h"  // LCG_CORE_LIB_API

#include <string>       // std::string
#include <stdexcept>    // std::runtime_error

namespace lcg{
namespace core{

/** @class System System.h lcg/core/System.h
 * @brief The System class contains static methods to interact with the underlying OS.
 * This class is useful to deal with OS specifics, like error codes, paths, etc.
 */
class LCG_CORE_LIB_API System {
public:

    /**
     * @brief Defines the type of Windows error to get.
     *  If on Linux or Mac OS, any value will default to ERRNO.
     */
    enum WinErrorType {
        WSAERROR,   /**< Used when working with sockets on Windows (involving a call to WSAStartup) */
        _DOSERRNO,  /**< Used when working with file I/O on Windows */
        ERRNO       /**< Used when not working with sockets or file I/O on windows. \n
                     This is the Default/Only option on Linux */
    };

    /**
     * @brief Gets the last error code set by a system call.
     * @param windowsErrorType Type of Windows error to get (ERRNO on Linux).
     * @return Returns the last error code set by a system Call. \n
     * This error code is thread's dependant therefore this method should be called as
     * soon as posible after an error is suspected of happening, because its value
     * can be changed by other intermediate system calls. \n
     * On error it rises an exception of type std::invalid_argument
     * @see WinErrorType
     */
    static int getLastErrorCode(WinErrorType windowsErrorType);

    /**
     * @brief Gets human-redable formatted string describing the error code.
     * @param errorCode The error code number codifing a "system call" error
     * (errno, _doserrno or WSAGetLastError), this depends of the OS platform, and
     * if Windows, the system call used (sockets, I/O, or neither).
     * @return Human-readable string describing the specifics of the error code.
     */
    static std::string getErrorStr(int errorCode);

    /**
     * @brief Gets current working directory.
     * The current working directory is defined as from where the binary
     * was called, not where it resides (see getExeDir()).
     * @return std::string containing the current working directory,
     * this is the directory upon which the executable was called. \n
     * Do not confuse this path with the path containing the executable. \n
     * On error it rises an exception of type std::runtime_error
     * @see getExeDir()
     */
    static std::string getCurrentWorkingDir();
    /**
     * @brief Gets the directory that holds the current executable program file.
     * @return std::string containing the directory that holds the current
     * executable program file. \n
     * Do not confuse this path with the path containing
     * the current working directory (see getCurrentWorkingDir()). \n
     * On error it rises an exception of type std::runtime_error
     * @see getCurrentWorkingDir()
     */
    static std::string getExeDir();

    #ifdef __WINDOWS
    /**
     * @brief Decodes the Windows error code into a human-readable string.
     * @note This method is only available on Windows.
     * @param errorCode The error code number codifing a system call error involving
     * WSAStartup or I/O operations on Windows (_doserrno or WSAGetLastError values).
     * @return Human-readable string describing the specifics of the error code.
     */
    std::string getWindowsErrorString(int errorCode);
    #endif // __WINDOWS

private:
    /////////////////////////////// EXCEPTIONS /////////////////////////////////
    static std::runtime_error getCurrentWorkingDir_getcwdError_Exception(int errorCode);
    static std::runtime_error getExeDir_getPathError_Exception(int errorCode);
    static std::invalid_argument getLastErrorCode_unknownWinErrorType_Exception(int windowsErrorType);

    System();   // Don't allow instantiations of this object.
};

} // namespace core
} // namespace lcg

#endif // LCG_CORE_SYSTEM_H
