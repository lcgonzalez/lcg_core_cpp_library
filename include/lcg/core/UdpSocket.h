// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file UdpSocket.h
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.6.2
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the LCG Core Cpp Library.
//
//                          License Agreement
//                     For the LCG Core Cpp Library
//
//    LCG Core Cpp Library is a lightweight open source library used to wrap
//    pthreads, Berkeley sockets, and other C libraries and functionalities,
//    into C++ objectes, over Linux, Mac OS and Windows.
//
//    LCG Core Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    LCG Core Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#ifndef LCG_CORE_UDPSOCKET_H
#define LCG_CORE_UDPSOCKET_H

#include "lcg/core/LCG_Core.h"  // LCG_CORE_LIB_API
#include "lcg/core/Core.h"      // lcg::core::Core

#ifdef __WINDOWS
#include <winsock2.h>   // scoket
#include <ws2tcpip.h>   // socket
//#pragma comment(lib, "ws2_32.lib") // Add library to Visual Studio Project
#endif // __WINDOWS
#ifdef __LINUX
#include <sys/types.h>  // bind (For compability and protability)
#include <sys/socket.h> // bind
#include <netinet/in.h> // sockaddr_in, in_addr, INET_ADDRSTRLEN
//#include <netinet/ip.h> // superset of previous
#include <arpa/inet.h>  // inet_ntop, inet_pton, ntohs, htons
#include <netdb.h>      // NI_NUMERICSERV
#include <cerrno>       // errno
#include <cstring>      // std::strerror
#include <unistd.h>     // close()
#endif // __LINUX

#include <string>       // std::string
#include <climits>      // INT_MAX
#include <stdexcept>    // std::runtime_error

namespace lcg{
namespace core{

#define TRYAGAIN -2 // Try to read a datagram again

/** @class UdpSocket UdpSocket.h lcg/core/UdpSocket.h
 * @brief The UdpSocket class creates a UDP socket to send data in unicast or multicast mode.
 * The socket can send or receive datagrams from and to one host (unicast), or
 * it can subscribe to a multicast channel to receive or send datagrams from and
 * to several hosts.
 *
 * The socket is easily managed by the bind(), joinMulticastGroup(), writeDatagram()
 * readDatagram() and close() methods.
 * @see bind() \n
 * joinMulticastGroup() \n
 * writeDatagram() \n
 * readDatagram() \n
 * close()
 */
class LCG_CORE_LIB_API UdpSocket : public Core{
public:
    /**
     * @brief Constructs a IPv4 UDP socket ready to be bound.
     * If the UdpSocket is going to be used for reading incoming data packets,
     * bind() should be called before readDatagram(). \n
     * If the UdpSocked is only going to be used for writing outgoing data packets,
     * the writeDatagram() may be called without a previous call to bind(). \n
     * On initalization error, it rises an exception of type std::runtime_error
     * @see bind() \n
     * readDatagram() \n
     * writeDatagram()
     */
    UdpSocket();

    /**
     * @brief Unbinds, closes and destroys the UdpSocket.
     * @see close()
     */
    ~UdpSocket();

    /**
     * @brief The BindMode enum defines the UdpSocket behaviour.
     * This variable defines if the UdpSsocket would be unique (DontShareAddress),
     * shared (ShareAddress), broadcast (SendBroadcast) or default for platform
     * (DefaultForPlaform).
     * @see bind() \n
     * setSocketOptions()
     */
    enum BindMode {
        DefaultForPlatform = 0x0, /**< The default option for the current platform.
            * On Unix and Mac OS X, this is equivalent to <em>DontShareAddress</em> \n
            * On Windows, it's equivalent to <em>ShareAddress</em>.
            */
        ShareAddress = 0x1,     /**< Allow other services to bind to the same address and port. */
        DontShareAddress = 0x2, /**< Bind the address and port exclusively, so that
            * no other services are allowed to rebind.
            */
        SendBroadcast = 0x4     /**< Configures socket for sending broadcast data. */
    };

    /**
     * @brief Binds the UdpSocket to <em>sZAddress</em>:<em>port</em> using a binding mode.
     * This method binds the UdpSocket to a specific addres, so the socket will only
     * listen to data incoming from that address. \n
     * This operation should be performed before reading any datagram using readDatagram().
     * However, if only sending datagrams, previous calls to this method can
     * be skipped and direct use of writeDatagram() is allowed.
     *
     * If binding to an already bound socket, an exception will be rised, so the
     * socket should be closed before a new bind() is attempted.
     * If the socket is closed, this method would attempt to create a new socket
     * and bind to the specific address and port.
     *
     * @param szAddress String that represents the IPv4 binding address. In
     * combination with the port, they represent the full qualified binding
     * address. \n
     * If using the special value szAddress="0.0.0.0", the socket will receive
     * all the traffic that arrives to the computer, without taking into account
     * the sender's IP address.
     * @param port Integer that represents the binding port. In combination with
     * the szAddress, they represent the full qualified binding address. \n
     * If left port=0, the socket will receive all the traffic that comes from
     * the binding address asAddress, without taking into account the port.
     * @param bindMode Defines the socket behaviour, whether it would be unique,
     * shared or broadcast.
     * @return 0 on success. \n
     * On failure it rises an exception of type std::runtime_error.
     * @see BindMode \n
     * readDatagram() \n
     * close()
     */
    int bind(std::string szAddress, int port=0, BindMode bindMode=DefaultForPlatform);

    /**
     * @brief Defines the behaviour of the UdpSocket.
     * The UdpSocket behaviour can be defined to be a unique socket (bind address
     * can't be used again), shared socket (bind address can be used again),
     * broadcast (socket sends broadcast data) or defined by the platform.
     * @param newBindMode Holds the new desired UdpSocket behaviour.
     * @return 0 on success. \n
     * On failure it rises an exception of type std::runtime_error.
     * @see BindMode \n
     * bind()
     */
    int setSocketOptions(BindMode newBindMode);

    /**
     * @brief Sets the timeout used by readDatagram() before returning (non blocking).
     * This time is used by readDatagram(), before returning with a TRYAGAIN value. \n
     * If sec=0 and usec=0, readDatagram() would be trated as a
     * blocking method, indefinitely blocking the thread execution until data
     * arrives to the bound address and port.
     * @note Not all implementations allow this option to be set. \n
     * Usually, the usec variable is ignored by the platform.
     * @param sec Seconds to timeout.
     * @param usec Micro seconds to timeout.
     * @return 0 on success. \n
     * On failure it rises an exception of type std::runtime_error.
     * @see readDatagram()
     */
    int setReadingTimeout(unsigned int sec, unsigned int usec);

    /**
     * @brief Joins to a multicast group without previous call to bind().
     * This method DOES NOT requiere a bound UdpSocket, so a previous call to bind()
     * is not requiered and should be avoided. All the binding will be carried
     * out by this method. \n
     * Trying to join to a bound socket will result in an exception rised.
     * @param szGroupAddress String that represents the IPv4 multicast group address.
     * @param szInterfaceAddress String that represents the IPv4 interface address of
     * the local machine. \n
     * This address defines the interface (network) that this UdpSocket will be joining.
     * @param port Integer that represents the port of the multicast group.
     * @return True on success. \n
     * On failure it rises an exception of type std::invalid_argument if the szGroupAddress argument
     * contains an invalid IPv4 dotted-decimal string or a invalid IPv6 address string. \n
     * Otherwise, it rises an exception of type std::runtime_error.
     */
    bool joinMulticastGroup(std::string szGroupAddress, std::string szInterfaceAddress, int port);

    /**
     * @brief Sends data to the desired address and port.
     * The UdpSocket doesn't need to be in a bound state (previous call to bind()),
     * to perform this operation.
     * @param data Pointer to buffer containing the data desired to be sent.
     * @param dataSize Number of bytes to send from the data buffer. \n
     * This number can't exceed INT_MAX (platform dependant), or an exception of
     * type std::overflow_error will be rised.
     * @param szAddress String form of the IPv4 destination addres where the
     * data will be sent.
     * @param port Port of the destination address.
     * @return On success, number of bytes sent to the destination address. \n
     * On failure, it rises an exception of type std::invalid_argument if the
     * dataSize argument contains an invalid argument (data size too big).
     * Otherwise, it rises and exception of type std::runtime_error.
     * @see UdpSocket
     */
    int writeDatagram(const char *data, size_t dataSize, const std::string szAddress, int port);

    /**
     * @brief Reads data from the previously bound address and port.
     * The UdpSocket DOES NEED to be in a bound state (previous call to bind()),
     * before a calling this method.
     * @param data Pointer to a buffer where the incoming data will be stored.
     * @param maxSize Number of bytes to read into the data buffer. \n
     * This number can't exceed INT_MAX (platform dependant), or an excpetion of
     * type std::overflow_error will be rised.
     * @param hostAddress IPv4 address in string form of the remote host machine
     * that sent the data.
     * @param port Port of the remote host machine that sent the data.
     * @return 0 on success. \n
     * TRYAGAIN if a timeout ocurred. \n
     * On failure, it rises an exception of type std::invalid_argument if the
     * maxSize argument contains an invalid argument (data size too big).
     * Otherwise, it rises and exception of type std::runtime_error.
     * @see bind() \n
     * setReadingTimeout()
     */
    int readDatagram(char *data, size_t maxSize, std::string &hostAddress, int *port);

    /**
     * @brief It's the same as readDatagram(), but without getting the IPv4
     * address and port of the remote host machine.
     * The UdpSocket DOES NEED to be in a bound state (previous call to bind()),
     * before a calling this method.
     * @param data Pointer to a buffer where the incoming data will be stored.
     * @param maxSize Number of bytes to read into the data buffer. \n
     * This number can't exceed INT_MAX (platform dependant), or an excpetion of
     * type std::overflow_error will be rised.
     * @return 0 on succes. \n
     * TRYAGAIN if a timeout ocurred. \n
     * On failure, it rises an exception of type std::invalid_argument if the
     * maxSize argument contains an invalid argument (data size too big).
     * Otherwise, it rises and exception of type std::runtime_error.
     * @see bind() \n
     * setReadingTimeout()
     */
    int readDatagram(char *data, size_t maxSize);

    /**
     * @brief Unbinds and closes the UdpSocket.
     * This is automatically called by the destructor, so no need to call it
     * unless wanting to rebind to other address/group.
     * @see ~UdpSocket
     */
    void close();

private:
    /**
     * @brief The SocketState enum Describes the state of the socket.
     * The socket coud be bound (BoundState), unbound (UnboundState) or
     * cosed (Closed).
     * @see
     */
    enum SocketState {
        UnboundState = 0x0, /**< The socket is not bound. */
        BoundState = 0x1,   /**< The socket is bound to an address and port. */
        Closed = 0x2        /**< The socket is closed. */
    };

    void createSocket();

    SocketState state_;
    unsigned int secTimeout_;
    unsigned int uSecTimeout_;

    /**
     * @brief joinMulticastGroup Joins to a multicast group requiring a previous call to bind.
     * This method requieres a bound socket, so a previous call to bind, with the
     * groupAddress as the binding address, is requiered.
     * @param szGroupAddress String form of the IPv4 multicast group address.
     * @param szInterfaceAddress String form of the IPv4 interface address of the
     * local machine, that defines the interface (or network) that this socket
     * will be joining.
     * @return True on success. \n
     * On failure it rises an exception of type std::invalid_argument if the szGroupAddress argument
     * contains an invalid IPv4 dotted-decimal string or a invalid IPv6 address string. \n
     * Otherwise, it rises an exception of type std::runtime_error.
     */
    bool joinMulticastGroup(std::string szGroupAddress, std::string szInterfaceAddress);
    int applyReadingTimeout();

    // convert ipp address string to addr
    bool IPAddressStringToAddr(const std::string szNameOrAddress, struct in_addr *Address);
    bool timeOutError();
    bool checkForSysInterruptionRecall(int errorCode);

    sockaddr_in destination;
    #ifdef __LINUX
    int socketID_;
    #endif // __LINUX
    #ifdef __WINDOWS
    SOCKET socketID_;
    #endif // __WINDOWS

    /////////////////////////////// EXCEPTIONS /////////////////////////////////
    std::runtime_error bind_SocketAlreadyBound_Exception();
    std::runtime_error bind_SocketNotBound_Exception(int errorCode);
    std::runtime_error createSocket_InvalidSocket_Exception(int errorCode);
    std::runtime_error setSocketOptions_setsockoptError_Exception(BindMode bindMode,
                                                                  int errorCode);
    std::runtime_error setReadingTimeout_setsockoptError_Exception(unsigned int sec,
                                                                   unsigned int usec,
                                                                   int errorCode);
    std::invalid_argument IPAddressStringToAddr_notValidMulticastStrAddress_Exception(std::string groupAddress);
    std::runtime_error IPAddressStringToAddr_destinationAddressFamilyError_Exception(std::string groupAddress,
                                                                                  int errorCode);
    std::runtime_error joinMulticastGroup_JoinGroupError_Exception(std::string groupAddress,
                                                                   int errorCode);
    std::runtime_error joinMulticastGroup_SetLocalInerfaceError_Exception(std::string interfaceAddress,
                                                                          int errorCode);
    std::invalid_argument writeDatagram_invalidDataSizeArgument_Exception();
    std::runtime_error writeDatagram_sendigDatagramError_Exception(int errorCode);
    std::invalid_argument readDatagram_overflowError_Exception();
    std::runtime_error readDatagram_ReadingDatagramError_Exception(int errorCode);
    std::runtime_error readDatagram_senderHostPortError_Exception(int errorCode);
    #ifdef __WINDOWS
    std::runtime_error createSocket_WSAerror_Exception(int WSAErrorCode);
    #endif // __WINDOWS
};

} // namespace core
} // namespace lcg

#endif // LCG_CORE_UDPSOCKET_H
