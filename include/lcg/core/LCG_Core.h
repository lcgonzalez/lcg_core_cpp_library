// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file LCG_Core.h
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.6.2
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the LCG Core Cpp Library.
//
//                          License Agreement
//                     For the LCG Core Cpp Library
//
//    LCG Core Cpp Library is a lightweight open source library used to wrap
//    pthreads, Berkeley sockets, and other C libraries and functionalities,
//    into C++ objectes, over Linux, Mac OS and Windows.
//
//    LCG Core Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    LCG Core Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#ifndef LCG_CORE_H
#define LCG_CORE_H

#ifdef __WINDOWS
#ifdef LCG_CORE_LIBRARY_EXPORTS
    #define LCG_CORE_LIB_API __declspec(dllexport)
#else
    #define LCG_CORE_LIB_API __declspec(dllimport)
#endif // LCG_CORE_LIBRARY_EXPORTS
#endif // __WINDOWS
#ifdef __LINUX
/**
 * @brief LCG_CORE_LIB_API is a useful macro to declare public classes or functions
 * to be accessible from outside the library on Windows.
 * On Linux or *nix like systems, there is no need to flag a class or function to be
 * accessible from outside, so on those platforms, the macro is empty.
 */
#define LCG_CORE_LIB_API
#endif // __LINUX

#ifdef __GNUC__
/**
 * @brief LCG_CORE_DEPRECATED is a useful macro to declare a function or metod deprecated.
 * It will issue a compiler warning if the developer uses a deprecated part
 * of the API, just like the java annotation "\@deprecated".
 **/
#define LCG_CORE_DEPRECATED(func) func __attribute__ ((deprecated))
#elif defined(_MSC_VER)
#define LCG_CORE_DEPRECATED(func) __declspec(deprecated) func
#else
#pragma message("WARNING: You need to implement DEPRECATED for this compiler")
#define LCG_CORE_DEPRECATED(func) func
#endif


/**
 * The lcg namespace is where all code developed by Luis Gonzalez resides.
 */
namespace lcg{
/**
 * The core namespace is where all the code from this library (LCG Core Cpp) resides.
 */
namespace core{

} // namespace core
} // namespace lcg


#endif // LCG_CORE_H
