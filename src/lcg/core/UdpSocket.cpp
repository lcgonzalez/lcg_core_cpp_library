// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file UdpSocket.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.6.2
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the LCG Core Cpp Library.
//
//                          License Agreement
//                     For the LCG Core Cpp Library
//
//    LCG Core Cpp Library is a lightweight open source library used to wrap
//    pthreads, Berkeley sockets, and other C libraries and functionalities,
//    into C++ objectes, over Linux, Mac OS and Windows.
//
//    LCG Core Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    LCG Core Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include "lcg/core/UdpSocket.h"
#include "lcg/core/System.h"    // lcg::core::System::getErrorStr, lcg::core::System::getLastErrorCode

#include <stdexcept>    // std::runtime_error
#include <sstream>      // std::ostringstream
#include <string>       // std::string

namespace lcg{
namespace core{

UdpSocket::UdpSocket() :
    state_(Closed),
    secTimeout_(0),     // Blocking default behaviour
    uSecTimeout_(0)     // Blocking default behaviour
{
    createSocket();
}

UdpSocket::~UdpSocket()
{
    close();
}

int UdpSocket::bind(std::string szAddress, int port, UdpSocket::BindMode bindMode)
{
    if(state_ == BoundState){
        throw bind_SocketAlreadyBound_Exception();
    }
    if(state_ == Closed){
        createSocket();
    }

    int errorCode = 0;
    errorCode = setSocketOptions(bindMode);
    if(errorCode != 0){
        return errorCode;
    }

    in_addr address;
    IPAddressStringToAddr(szAddress, &address);
    // Set binding port
    destination.sin_port = htons(port);
    // Set binding address
    #ifdef __WINDOWS
    destination.sin_addr.S_un.S_addr = address.S_un.S_addr;
    #endif // __WINDOWS
    #ifdef __LINUX
    destination.sin_addr.s_addr = address.s_addr;
    #endif // __LINUX

    errorCode = ::bind(socketID_, (struct sockaddr *)&destination, sizeof(struct sockaddr));
    if(errorCode != 0) {
        // On Error
        errorCode = System::getLastErrorCode(System::WSAERROR);
        close();
        throw bind_SocketNotBound_Exception(errorCode);
    }

    state_ = BoundState;
    applyReadingTimeout();
    return errorCode;
}

void UdpSocket::createSocket()
{
    destination.sin_family = AF_INET;

    #ifdef __WINDOWS
    // Call WSAStartup for sockets' API usage
    WORD wVersionRequested;
    WSADATA wsaData;
    int wsaErrorCode;

    // Using MAKEWORD macro, Winsock version request 2.2
    wVersionRequested = MAKEWORD(2, 2);

    wsaErrorCode = WSAStartup(wVersionRequested, &wsaData);
    if (wsaErrorCode != 0)
    {
        WSACleanup();
        throw createSocket_WSAerror_Exception(wsaErrorCode);

    }
    #endif // __WINDOWS

    bool invalidSocket = false;
    int errorCode = 0;
    // Create a blocking, datagram socket
    socketID_ = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    errorCode = System::getLastErrorCode(System::WSAERROR);
    #ifdef __WINDOWS
    if(socketID_ == INVALID_SOCKET) {
        invalidSocket = true;
    }
    #endif // __WINDOWS
    #ifdef __LINUX
    if(socketID_ == -1) {
        invalidSocket = true;
    }
    #endif // __LINUX

    //Rise exception of invalid socket.
    if(invalidSocket){
        throw createSocket_InvalidSocket_Exception(errorCode);
    }
    state_ = UnboundState;
}

void UdpSocket::close()
{
    if(state_ != Closed ) {
        #ifdef __WINDOWS
        closesocket(socketID_);
        WSACleanup();
        #endif // __WINDOWS
        #ifdef __LINUX
        ::close(socketID_);
        #endif // __LINUX
        state_ = Closed;
    }
}

bool UdpSocket::IPAddressStringToAddr(const std::string szNameOrAddress, in_addr *Address)
{
    int errorCode;
    u_short port;
    port = 0;

    //NOTE: Long time to connect fixed. getnameinfo function not needed.
    errorCode = inet_pton(destination.sin_family, szNameOrAddress.c_str(), Address);
    if(errorCode == 0){ // Not valid valid IPv4 dotted-decimal string or a valid IPv6 multicast address string
        close();
        throw IPAddressStringToAddr_notValidMulticastStrAddress_Exception(szNameOrAddress);
    } else if( errorCode == -1) {   // destination.sin_family does not contain a valid address family or other error
        errorCode = System::getLastErrorCode(System::WSAERROR);
        close();
        throw IPAddressStringToAddr_destinationAddressFamilyError_Exception(szNameOrAddress, errorCode);
    }
    return true;
}

bool UdpSocket::timeOutError()
{
    #ifdef __LINUX
    if((errno == EAGAIN) || (errno == EWOULDBLOCK)){
        return true;
    }
    #endif // __LINUX
    //WARNING: Add the Windows code
    #ifdef __WINDOWS
    #endif // __WINDOWS
    return false;
}

bool UdpSocket::checkForSysInterruptionRecall(int errorCode)
{
    #ifdef __LINUX
    if( (errorCode == -1) && (errno == EINTR)){
       return true;
    } else {
       return false;
    }
    #endif // __LINUX
    //WARNING: Add the Windows code
    #ifdef __WINDOWS
    #endif // __WINDOWS
   return false;
}

int UdpSocket::setSocketOptions(BindMode newBindMode)
{
    BindMode bindMode = newBindMode;
    int ivalue = 0;
    int errorCode = 0;

    if(bindMode == DefaultForPlatform){
        #ifdef __WINDOWS
        // For Windows
        bindMode = (BindMode)(bindMode | ShareAddress);
        #endif // __WINDOWS
        #ifdef __LINUX
        // For Linux
        bindMode = (BindMode)(bindMode | DontShareAddress);
        #endif // __LINUX
    }
    if((bindMode & ShareAddress) == ShareAddress){
        ivalue = 1;
        errorCode = setsockopt(socketID_, SOL_SOCKET, SO_REUSEADDR, (char *)&ivalue, sizeof(ivalue));
        if(errorCode != 0){
            // On Error
            errorCode = System::getLastErrorCode(System::WSAERROR);
            close();
            throw setSocketOptions_setsockoptError_Exception(ShareAddress, errorCode);
        }
        #ifdef __WINDOWS
        ivalue = 0;
        errorCode = setsockopt(socketID_, SOL_SOCKET, SO_EXCLUSIVEADDRUSE, (char *)&ivalue, sizeof(ivalue));
        if(errorCode != 0){
            // On Error
            errorCode = System::getLastErrorCode(System::WSAERROR);
            close();
            throw setSocketOptions_setsockoptError_Exception(ShareAddress, errorCode);
        }
        #endif // __WINDOWS
    }
    if((bindMode & DontShareAddress) == DontShareAddress){
        ivalue = 0;
        errorCode = setsockopt(socketID_, SOL_SOCKET, SO_REUSEADDR, (char *)&ivalue, sizeof(ivalue));
        if(errorCode != 0){
            // On Error
            errorCode = System::getLastErrorCode(System::WSAERROR);
            close();
            throw setSocketOptions_setsockoptError_Exception(DontShareAddress, errorCode);
        }
        #ifdef __WINDOWS
        ivalue = 1;
        errorCode = setsockopt(socketID_, SOL_SOCKET, SO_EXCLUSIVEADDRUSE, (char *)&ivalue, sizeof(ivalue));
        if(errorCode != 0){
            // On Error
            errorCode = System::getLastErrorCode(System::WSAERROR);
            close();
            throw setSocketOptions_setsockoptError_Exception(DontShareAddress, errorCode);
        }
        #endif // __WINDOWS
    }
    if((bindMode & SendBroadcast) == SendBroadcast){
        ivalue = 1;
        errorCode = setsockopt(socketID_, SOL_SOCKET, SO_BROADCAST, (char *)&ivalue, sizeof(ivalue));
        if(errorCode != 0){
            // On Error
            errorCode = System::getLastErrorCode(System::WSAERROR);
            close();
            throw setSocketOptions_setsockoptError_Exception(SendBroadcast, errorCode);
        }
    }
    return errorCode;
}

int UdpSocket::setReadingTimeout(unsigned int sec, unsigned int usec)
{
    secTimeout_ = sec;
    uSecTimeout_ = usec;

    if(state_ == BoundState) {
        return applyReadingTimeout();
    }
    return 0;
}

bool UdpSocket::joinMulticastGroup(std::string szGroupAddress, std::string szInterfaceAddress)
{
    int errorCode = 0;

    in_addr interfaceAddress;
    IPAddressStringToAddr(szInterfaceAddress, &interfaceAddress);

    in_addr multiCastAddress;
    IPAddressStringToAddr(szGroupAddress, &multiCastAddress);

    ip_mreq Mreq;
    Mreq.imr_multiaddr = multiCastAddress;
    Mreq.imr_interface = interfaceAddress;

    // Join the multicast group "multiCastAddress" on the interface "interfaceAddress".
    errorCode = setsockopt(socketID_, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&Mreq, sizeof(Mreq));
    if(errorCode != 0){
        // On Error
        errorCode = System::getLastErrorCode(System::WSAERROR);
        close();
        throw joinMulticastGroup_JoinGroupError_Exception(szGroupAddress, errorCode);
    }
    // Set local interface to "interfaceAddress" for outbound multicast datagrams.
    errorCode = setsockopt(socketID_, IPPROTO_IP, IP_MULTICAST_IF, (char *)&interfaceAddress, sizeof(interfaceAddress));
    if(errorCode != 0){
        // On Error
        errorCode = System::getLastErrorCode(System::WSAERROR);
        close();
        throw joinMulticastGroup_SetLocalInerfaceError_Exception(szInterfaceAddress, errorCode);
    }
    applyReadingTimeout();
    return true;
}

int UdpSocket::applyReadingTimeout()
{
    int errorCode = 0;

    if((secTimeout_ == 0) && (uSecTimeout_ == 0)){ // No timeout
    }

    struct timeval tv;
    tv.tv_sec = secTimeout_;
    tv.tv_usec = uSecTimeout_;
    errorCode = setsockopt(socketID_, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));
    if(errorCode != 0){
        // On Error
        errorCode = System::getLastErrorCode(System::WSAERROR);
        close();
        throw setReadingTimeout_setsockoptError_Exception(secTimeout_, uSecTimeout_, errorCode);
    }
    return errorCode;
}

bool UdpSocket::joinMulticastGroup(std::string szGroupAddress, std::string szInterfaceAddress, int port)
{
    try {
		// NOTE: The binding address for joining a multicast group changes between platforms according to this:
		// Windows: The binding address should be either, the wildcard IPv4 address (INADDR_ANY), or the wildcard IPv6 address (in6addr_any). 
		// Unix: The binding address should be the Multicast group.
        #ifdef __WINDOWS
        this->bind(szInterfaceAddress, port, UdpSocket::ShareAddress);
        #endif // __WINDOWS
        #ifdef __LINUX
        this->bind(szGroupAddress, port, UdpSocket::ShareAddress);
        #endif // __LINUX
		
    } catch(std::runtime_error &e) {
        std::ostringstream oss;
        oss << "Binding error while joining multicast group: " << e.what();
        throw std::runtime_error(oss.str());
    }
    return joinMulticastGroup(szGroupAddress, szInterfaceAddress);
}

int UdpSocket::writeDatagram(const char *data, size_t dataSize, const std::string szAddress, int port)
{
	if (dataSize > INT_MAX){
        throw writeDatagram_invalidDataSizeArgument_Exception();
	}
	int convertDataSize = static_cast<int>(dataSize);

    in_addr destinationAddress;
    IPAddressStringToAddr(szAddress, &destinationAddress);
    sockaddr_in hostAddress;
    hostAddress.sin_family = AF_INET;
    hostAddress.sin_port = htons(port);
    hostAddress.sin_addr = destinationAddress;

    int errorCode = 0;
    int nDataBytesSent = 0;
	nDataBytesSent = sendto(socketID_, data, convertDataSize, 0, (sockaddr *)&hostAddress, sizeof(hostAddress));
    // Useful if an error ocurred
    errorCode = System::getLastErrorCode(System::WSAERROR);

    //Rise exception if error while writing a datagram.
    #ifdef __WINDOWS
    if(nDataBytesSent == SOCKET_ERROR){
    #endif // __WINDOWS
    #ifdef __LINUX
    if(nDataBytesSent < 0){
    #endif // __LINUX
        throw writeDatagram_sendigDatagramError_Exception(errorCode);
    }
    return nDataBytesSent;

}

int UdpSocket::readDatagram(char *data, size_t maxSize, std::string &hostAddress, int *port)
{
	if (maxSize > INT_MAX){
        throw readDatagram_overflowError_Exception();
	}
	int convertMaxSize = static_cast<int>(maxSize);

    int nDataBytesReceived = 0;
    sockaddr_in senderHostAddress;
    #ifdef __WINDOWS
    int addrLength = sizeof(struct sockaddr);
    #endif // __WINDOWS
    #ifdef __LINUX
    socklen_t addrLength = sizeof(struct sockaddr);
    #endif // __LINUX
    // Block until a datagram is received from the network (from anyone including the local machine)
    // If interrupetd, recall the system call (recvfrom).
    // This is useful when using a debugger, because the system call can be interrupted.
    bool recall = false;
    do {
        nDataBytesReceived = recvfrom(socketID_, data, convertMaxSize, 0, (sockaddr *)&senderHostAddress, &addrLength);
        recall = checkForSysInterruptionRecall(nDataBytesReceived);
    }while( recall );

    int errorCode = 0;
    // Useful if an error ocurred
    errorCode = System::getLastErrorCode(System::WSAERROR);

    //Rise exception if error while reading a datagram.
    #ifdef __WINDOWS
    if(nDataBytesReceived == SOCKET_ERROR){
    #endif // __WINDOWS
    #ifdef __LINUX
    if(nDataBytesReceived < 0){
    #endif // __LINUX
        if(timeOutError()){
            return -2; // ReadDatagram Timeout
        }
        throw readDatagram_ReadingDatagramError_Exception(errorCode);
    }

    //hostAddress = inet_ntoa(senderHostAddress.sin_addr);
	char str[INET_ADDRSTRLEN];

    const char* strError = inet_ntop(destination.sin_family, &(senderHostAddress.sin_addr), str, INET_ADDRSTRLEN);
    if(strError == NULL){
        // On error
        errorCode = System::getLastErrorCode(System::WSAERROR);
        throw readDatagram_senderHostPortError_Exception(errorCode);
    }
	hostAddress = str;

    if(port != NULL){
        *port = ntohs(senderHostAddress.sin_port);
    }
    return 0;
}

int UdpSocket::readDatagram(char *data, size_t maxSize)
{
    std::string dummy;
    return readDatagram(data, maxSize, dummy, NULL);
}

/////////////////////////// EXCEPTIONS /////////////////////////////////////////

std::runtime_error UdpSocket::bind_SocketAlreadyBound_Exception()
{
    char str[INET_ADDRSTRLEN];
    inet_ntop(destination.sin_family, &(destination.sin_addr), str, INET_ADDRSTRLEN);
    std::ostringstream oss;
    oss << "Error while binding socket. Socket already bound to: "
       << str << ":" << ntohs(destination.sin_port) << ". "
       << "Please close the socket before rebinding!!";
    return std::runtime_error(oss.str());
}

std::runtime_error UdpSocket::bind_SocketNotBound_Exception(int errorCode)
{
    std::ostringstream oss;
    oss << "Error while binding socket. Closing Socket " << socketID_ << ". " <<
           System::getErrorStr(errorCode);
    return std::runtime_error(oss.str());
}

std::runtime_error UdpSocket::createSocket_InvalidSocket_Exception(int errorCode)
{
    std::ostringstream oss;
    oss << "Error while creating socket. Invalid socket! " <<
           System::getErrorStr(errorCode);
    return std::runtime_error(oss.str());
}

std::runtime_error UdpSocket::setSocketOptions_setsockoptError_Exception(BindMode bindMode,
                                                                         int errorCode)
{
    std::string socketOptionStr;
    switch(bindMode){
    case ShareAddress:
        socketOptionStr = "ShareAddress";
        break;
    case DontShareAddress:
        socketOptionStr = "DontShareAddress";
        break;
    default:
        socketOptionStr = "Unknown_Socket_Option";
        break;
    }

    std::ostringstream oss;
    oss << "Error while setting the socket option to " << socketOptionStr << ". "
           "Closing Socket " << socketID_ << ". " << System::getErrorStr(errorCode);
    return std::runtime_error(oss.str());
}

std::runtime_error UdpSocket::setReadingTimeout_setsockoptError_Exception(unsigned int sec,
                                                                          unsigned int usec,
                                                                          int errorCode)
{
    std::ostringstream oss;
    oss << "Error while setting the reading timeout to " << sec << "seconds and " <<
           usec << "microseconds. Closing Socket " << socketID_ << ". " <<
           System::getErrorStr(errorCode);
    return std::runtime_error(oss.str());
}

std::invalid_argument UdpSocket::IPAddressStringToAddr_notValidMulticastStrAddress_Exception(std::string groupAddress)
{
    std::ostringstream oss;
    oss << "Error, socket binding address " << groupAddress << " couldn't be defined. " << std::endl <<
           "The address string is not a valid IPv4 dotted-decimal string, a valid IPv6 address string, " <<
           "or a string representing a valid network address in the specified address family. Closing Socket" <<
           socketID_ << ".";
    return std::invalid_argument(oss.str());
}

std::runtime_error UdpSocket::IPAddressStringToAddr_destinationAddressFamilyError_Exception(std::string groupAddress, int errorCode)
{
    std::ostringstream oss;
    oss << "Error, socket binding address " << groupAddress << " couldn't be defined. " << std::endl <<
           "There was an error related to the destination address family. "
           "Closing Socket " << socketID_ << ". " << System::getErrorStr(errorCode);
    return std::runtime_error(oss.str());
}

std::runtime_error UdpSocket::joinMulticastGroup_JoinGroupError_Exception(std::string groupAddress,
                                                                          int errorCode)
{
    std::ostringstream oss;
    oss << "Error while joining the multicast group " << groupAddress << "; " <<
           "Closing Socket " << socketID_ << ". " << System::getErrorStr(errorCode);
    return std::runtime_error(oss.str());
}

std::runtime_error UdpSocket::joinMulticastGroup_SetLocalInerfaceError_Exception(std::string interfaceAddress,
                                                                                 int errorCode)
{
    std::ostringstream oss;
    oss << "Error while setting the local interface " << interfaceAddress << "; " <<
           "for outbound multicast datagrams (Joining multicast group). Closing Socket " <<
           socketID_ << ". " << System::getErrorStr(errorCode);
    return std::runtime_error(oss.str());
}

std::invalid_argument UdpSocket::writeDatagram_invalidDataSizeArgument_Exception()
{
    std::ostringstream oss;
    oss << "UdpSocket::writeDatagram dataSize parameter is larger than INT_MAX: " << INT_MAX;
    return std::invalid_argument(oss.str());
}

std::runtime_error UdpSocket::writeDatagram_sendigDatagramError_Exception(int errorCode)
{
    std::ostringstream oss;
    oss << "Error while sending datagram. Closing Socket " << socketID_ << ". " <<
           System::getErrorStr(errorCode);
    return std::runtime_error(oss.str());
}

std::invalid_argument UdpSocket::readDatagram_overflowError_Exception()
{
    std::ostringstream oss;
    oss << "UdpSocket::readDatagram maxSize parameter is larger than INT_MAX: " << INT_MAX;
    return std::invalid_argument(oss.str());
}

std::runtime_error UdpSocket::readDatagram_ReadingDatagramError_Exception(int errorCode)
{
    std::ostringstream oss;
    oss << "Error while reading datagram. Closing Socket " << socketID_ << ". " <<
           System::getErrorStr(errorCode);
    return std::runtime_error(oss.str());
}

std::runtime_error UdpSocket::readDatagram_senderHostPortError_Exception(int errorCode)
{
    std::ostringstream oss;
    oss << "Error while reading datagram." << std::endl <<
           "The sender port coudn't be obtained. " <<
           System::getErrorStr(errorCode);
    return std::runtime_error(oss.str());
}

#ifdef __WINDOWS
std::runtime_error UdpSocket::createSocket_WSAerror_Exception(int WSAErrorCode)
{
    std::string wsaErrorStr;
    switch (WSAErrorCode){
    case WSASYSNOTREADY :
        wsaErrorStr = "WSASYSNOTREADY : The underlying network subsystem is not "
                      "ready for network communication.";
        break;
    case WSAVERNOTSUPPORTED :
        wsaErrorStr = "WSAVERNOTSUPPORTED : The version of Windows Sockets support "
                      "requested is not provided by this particular Windows Sockets "
                      "implementation.";
        break;
    case WSAEINPROGRESS :
        wsaErrorStr = "WSAEINPROGRESS : A blocking Windows Sockets 1.1 operation "
                      "is in progress.";
        break;
    case WSAEPROCLIM :
        wsaErrorStr = "WSAEPROCLIM : A limit on the number of tasks supported by "
                      "the Windows Sockets implementation has been reached.";
        break;
    case WSAEFAULT :
        wsaErrorStr = "WSAEFAULT : The lpWSAData parameter is not a valid pointer.";
        break;
    default :
        wsaErrorStr = "Unknown Error.";
        break;
    }
    std::ostringstream tmpOss;
    tmpOss << "WSAStartup failed! Error code # " << WSAErrorCode << " [" << wsaErrorStr << "]";
    return std::runtime_error(tmpOss.str());
}
#endif // __WINDOWS

} // namespace core
} // namespace lcg
