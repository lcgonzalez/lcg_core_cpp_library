// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file OSSpecificFileParser.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.6.2
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the LCG Core Cpp Library.
//
//                          License Agreement
//                     For the LCG Core Cpp Library
//
//    LCG Core Cpp Library is a lightweight open source library used to wrap
//    pthreads, Berkeley sockets, and other C libraries and functionalities,
//    into C++ objectes, over Linux, Mac OS and Windows.
//
//    LCG Core Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    LCG Core Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include "lcg/core/OSSpecificFileParser.h"
#include "lcg/core/System.h"    // lcg::core::System::getLastErrorCode, lcg::core::System::getErrorStr

#include <fstream>  // std::fstream
#include <iostream> // std::fstream
#include <string>   // std::string
#include <sstream>  // std::ostreamstring

namespace lcg{
namespace core{

void OSSpecificFileParser::populateMacroStings()
{
    genericStartMacro_ = "#ifdef";
    genericEndMacroStr_ = "#endif";

    cloneStr_ = "clone";
}

OSSpecificFileParser::OSSpecificFileParser() : filePath_("") {
    populateMacroStings();
}

OSSpecificFileParser::OSSpecificFileParser(std::string filePath) :
    filePath_(filePath)
{
    populateMacroStings();
}

OSSpecificFileParser::~OSSpecificFileParser() {
    if( file_.is_open() ) {
        file_.close();
    }
}

void OSSpecificFileParser::setInputFilePath(std::string newFilePath)
{
    filePath_ = newFilePath;
}

void OSSpecificFileParser::addPlatformMacro(std::string platformMacro)
{
    platformsMacros_.push_back(platformMacro);
}

void OSSpecificFileParser::clearPlatformMacros()
{
    platformsMacros_.clear();
}

int OSSpecificFileParser::parse(std::string platformMacroString)
{
    if( !file_.is_open() ) {    // Open if file is closed.
        file_.open(filePath_.c_str());
        int errorCode = System::getLastErrorCode(System::ERRNO);
        if( !file_.good() ) {
            throw parseFile_FileIsNotOpen_Exception(errorCode, filePath_);
        }
    }
    if( platformMacroString == cloneStr_) { // If platform macro string is "clone", clone file and return inmediatly
        ss_ << file_.rdbuf();
        return 0;
    } else { // Identify if the platform macro strings is a known one
        bool knownPlatformMacro = false;
        for(std::vector<std::string>::iterator strIt = platformsMacros_.begin();
            strIt != platformsMacros_.end(); strIt++){
            if( *strIt == platformMacroString ){
                knownPlatformMacro = true;
                break;
            }
        }
        if( !knownPlatformMacro ) {
            throw parseFile_UnknownPlatformMacroString_Exception(platformMacroString);
        }
    }

    ss_.str(std::string()); // Clear stringstream and prepare it for writing parsed file
    std::string currentLine;

    int openingMacroNum = 0;
    bool copyLine = true;
    bool avoidLine = false;
    bool betweenPlatformMacro = false;
    while( std::getline(file_, currentLine) ) {
        if( !betweenPlatformMacro ) {   // If not between a known platform macro
            if( currentLine.find(genericStartMacro_) != std::string::npos ){ // If there is a "#ifdef" in this line
                for(std::vector<std::string>::iterator strIt = platformsMacros_.begin();
                    strIt != platformsMacros_.end(); strIt++) {  // Test for all known platform macros
                    std::string otherPlatformMacroStr = genericStartMacro_ + " " + *strIt;
                    if( currentLine.find(otherPlatformMacroStr) != std::string::npos ) { // If the line contains a known platform macro
                        betweenPlatformMacro = true;
                        avoidLine = true;   // Skip the starting "#ifdef" platform macro line
                        if( *strIt != platformMacroString ) {   // If the platform macro is different than the current platform
                            copyLine = false;
                        }
                        break;
                    }
                }   // for loop end
            }
        } else {    // If between a known platform macro
            if( currentLine.find(genericStartMacro_) != std::string::npos ){     // Count number of nested "#ifdef" macros
                openingMacroNum++;
            } else if( currentLine.find(genericEndMacroStr_) != std::string::npos ){    // Count number of nested "#endif" macros
                if( openingMacroNum <= 0 ) {    // No more nested "#endif", return to copy lines.
                    avoidLine = true;   // Skip the closing "#endif" macro line
                    copyLine = true;
                    betweenPlatformMacro = false;
                } else {                        // Nested #endif, do not return to copy lines.
                    openingMacroNum--;
                }
            }
        }

        if( copyLine ) {
            if( avoidLine ) {
                avoidLine = false;
            } else {
                ss_ << currentLine << std::endl;
            }
        }
    }
    return 0;
}

int OSSpecificFileParser::writeToPath(std::string writingPath)
{
    std::ofstream outputFile;
    outputFile.open(writingPath.c_str(), std::ios::out | std::ios::trunc);
    int errorCode = System::getLastErrorCode(System::ERRNO);
    if( !outputFile.is_open() ) {
        throw writeToPath_FileNotOpened_Exception(errorCode, writingPath);
    }
    if( ss_.str().empty() ) {
        throw writeToPath_writingNotParsedFile_Exception();
    }
    outputFile << ss_.rdbuf();  // I/O stringstream needed for this
    return 0;
}

int OSSpecificFileParser::compareTextFiles(std::string filePath_A, std::string filePath_B)
{
    std::ifstream file_A, file_B;

    // Open files at the end (ate) of the file, in order to compare the files' sizes
    file_A.open(filePath_A.c_str(), std::ios::in | std::ios::ate | std::ios::binary);
    int errorCode_A = System::getLastErrorCode(System::ERRNO);
    if( !file_A.is_open() ) {
        throw parseFile_FileIsNotOpen_Exception(errorCode_A, filePath_A);
    }

    file_B.open(filePath_B.c_str(), std::ios::in | std::ios::ate | std::ios::binary);
    int errorCode_B = System::getLastErrorCode(System::ERRNO);
    if( !file_B.is_open() ) {
        throw parseFile_FileIsNotOpen_Exception(errorCode_B, filePath_B);
    }

    if(file_A.tellg() != file_B.tellg()) { // If files' sizes are different, files are different
        return -1;
    }

    // Close and reopen files at the begining of the file, in order to compare
    // line by line.
    file_A.close();
    file_B.close();
    file_A.open(filePath_A.c_str());
    file_B.open(filePath_B.c_str());
    std::string line_A, line_B;
    while( std::getline(file_A, line_A) ) { // Correct way of checking if more data, don't use .eof() method
        std::getline(file_B, line_B);
        if ( line_A.compare(line_B) != 0  ) {   // If any line is different.
            return -1;
        }
    }

    return 0;   // Both text files are equal.
}

/////////////////////////// EXCEPTIONS /////////////////////////////////////////

std::runtime_error OSSpecificFileParser::parseFile_FileIsNotOpen_Exception(int errorCode, std::string filePath)
{
    std::ostringstream oss;
    oss << "Error while openning the file: " << filePath << ". " <<
           System::getErrorStr(errorCode);
    return std::runtime_error(oss.str());
}

std::runtime_error OSSpecificFileParser::writeToPath_FileNotOpened_Exception(int errorCode, std::string filePath)
{
    std::ostringstream oss;
    oss << "Error while creating the file: " << filePath << ". " <<
           System::getErrorStr(errorCode);
    return std::runtime_error(oss.str());
}

std::runtime_error OSSpecificFileParser::writeToPath_writingNotParsedFile_Exception()
{
    std::ostringstream oss;
    oss << "Error while writing parsed file to disk: Attempting to write a file " <<
           "that has not yet been parsed.";
    return std::runtime_error(oss.str());
}

std::domain_error OSSpecificFileParser::parseFile_UnknownPlatformMacroString_Exception(std::string unknownPlatformMacroString)
{
    std::ostringstream oss;
    oss << "Error while parsing file: unknown platform macro string \"" <<
           unknownPlatformMacroString << "\"";
    return std::domain_error(oss.str());
}

}   // namespace core
}   // namespace lcg
