// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file Core.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.5.1
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the LCG Core Cpp Library.
//
//                          License Agreement
//                     For the LCG Core Cpp Library
//
//    LCG Core Cpp Library is a lightweight open source library used to wrap
//    pthreads, Berkeley sockets, and other C libraries and functionalities,
//    into C++ objectes, over Linux, Mac OS and Windows.
//
//    LCG Core Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    LCG Core Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include "lcg/core/Core.h"

#include <sstream>  // std::ostringstream
#include <iostream> // std::ostream, std::cout
#include <limits>   // std::numeric_limits

namespace lcg{
namespace core{

Core::Core() :
    name_("no_name_set"),
    ID_(-1)
{
}

Core::~Core()
{
}

void Core::print() const
{
    std::cout << this;
}

bool Core::isEqual(const Core& other) const
{
    if(ID_ != other.getID()) {
        return false;
    }
    if(name_.compare(other.getName()) != 0){
        return false;
    }
    return true;
}

void Core::setName(const std::string& newName)
{
    name_ = newName;
}

std::string Core::getName() const
{
    return name_;
}

void Core::setID(const int& newID)
{
    ID_ = newID;
}

int Core::getID() const
{
    return ID_;
}

bool Core::operator==(const Core& other) const
{
    return isEqual(other);
}

bool Core::operator !=(const Core& other) const
{
    return !isEqual(other);
}

std::ostream& operator<<(std::ostream& os, const Core& core)
{
    os << &core;
    return os;
}

std::ostream& operator<<(std::ostream& os, const Core* core)
{
    if(core==NULL){
        throw Core::print_printNullObject_Exception();
    }
    os << "Object address=[" << static_cast<const void*const>(core) << "] Name: "
       << core->name_ << ", ID: " << core->ID_;
    return os;
}

/////////////////////////// EXCEPTIONS /////////////////////////////////////////

std::runtime_error Core::print_printNullObject_Exception()
{
    return std::runtime_error("Attempting to print null pointer to Core object");
}

} // namespace core
} // namespace lcg
