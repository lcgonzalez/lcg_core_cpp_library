// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file Thread.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.6.2
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the LCG Core Cpp Library.
//
//                          License Agreement
//                     For the LCG Core Cpp Library
//
//    LCG Core Cpp Library is a lightweight open source library used to wrap
//    pthreads, Berkeley sockets, and other C libraries and functionalities,
//    into C++ objectes, over Linux, Mac OS and Windows.
//
//    LCG Core Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    LCG Core Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include "lcg/core/Thread.h"
#include "lcg/core/System.h"    // lcg::core::System::getErrorStr

#include <sstream>  // std::ostringstream
#include <cstring>  // std::memcpy

#ifdef __LINUX
#include <cerrno>   // errno
#endif // __LINUX

namespace lcg{
namespace core{

namespace C_ {  // The namespace C_ is a hack for the compiler not to complain
extern "C"
void *startThread(void* arg) {
    Thread *thread = reinterpret_cast<Thread *>(arg);
    thread->run();
    thread->threadState_ = Thread::Finished;
    pthread_exit(NULL); // No erros, always succeds
}
}

Thread::Thread() :
    threadID_(0),
    threadState_(UnInitialized)
{
}

Thread::~Thread()
{
}

int Thread::start()
{
    int retValue = 0;
    switch (threadState_) {
    case UnInitialized:
    case Finished:
        retValue = pthread_create(&threadID_, NULL, C_::startThread, this);
        if(retValue != 0){ // Error
            threadID_ = 0;
            throw start_pthreadCreateError_Exception(retValue);
        } else {
            threadState_ = Running;
        }
        break;
    case Running:
        break;
    default:
        throw start_unknownThreadState_Exception();
    }
    return retValue;
}

bool Thread::isFinished()
{
    return (threadState_ == Finished);
}

bool Thread::isRunning()
{
    return (threadState_ == Running);
}

int Thread::wait()
{
    int retValue = 0;
    if(threadState_ == Running) {
        retValue = pthread_join(threadID_, NULL);
        if(retValue != 0){ // Error
            threadID_ = 0;
            exit();
            throw wait_pthreadJoinError_Exception(retValue);
        } else {
            threadState_ = Finished;
        }
        return retValue;
    } else {
        return retValue;
    }
}

/////////////////////////// EXCEPTIONS /////////////////////////////////////////

std::runtime_error Thread::start_pthreadCreateError_Exception(int errorCode)
{
    #ifdef __LINUX
        std::string errorStr;
        switch(errorCode){
        case EAGAIN:
            errorStr = "Insufficient resources to create another thread. "
                    "A system-imposed limit on the number of threads was "
                    "encountered.";
            break;
        case EINVAL:
            errorStr = "EINVAL : Invalid settings in attr.";
            break;
        case EPERM:
            errorStr = "EPERM : No permission to set the scheduling policy and "
                    "parameters specified in attr.";
            break;
        default:
            errorStr = System::getErrorStr(errorCode);
        }
        std::ostringstream oss;
        oss << "Thread::start failed! Error code # " << errorCode << " [" << errorStr << "]";
    #endif // __LINUX
    return std::runtime_error(oss.str());
}

std::runtime_error Thread::wait_pthreadJoinError_Exception(int errorCode)
{
    #ifdef __LINUX
        std::string errorStr;
        switch(errorCode){
        case EDEADLK:
            errorStr = "A deadlock was detected (e.g., two threads tried "
                    "to join with each other); or thread specifies the calling thread.";
            break;
        case EINVAL:
            errorStr = "This thread is not a joinable thread, or another thread "
                    "is already waiting to join with this thread.";
            break;
        case ESRCH:
            errorStr = "This thread has an invalid ID, no thread with that ID could be found.";
            break;
        default:
            errorStr = System::getErrorStr(errorCode);
        }
        std::ostringstream oss;
        oss << "Thread::wait failed! Error code # " << errorCode << " [" << errorStr << "]";
    #endif // __LINUX
    return std::runtime_error(oss.str());
}

std::runtime_error Thread::start_unknownThreadState_Exception()
{
    std::ostringstream oss;
    oss << "Error while starting a new thread. The current state of the thread " <<
           "is not defined by a value of ThreadState enum";
    return std::runtime_error(oss.str());
}

// Default Constructor
Mutex::Mutex() :
    locked_(false), lock_(new pthread_mutex_t)
{
    int retVal=0;
    retVal = pthread_mutex_init(lock_, NULL);
    if(retVal != 0) {   // On error
        throw Mutex_pthreadMutexInitError_Exception(retVal);
    }
}

// Copy Constructor
Mutex::Mutex (const Mutex& other) :
    locked_(other.locked_), lock_(other.lock_)
{
    //numberOfMutexes += 1;
}

// Copy assignment operator
Mutex& Mutex::operator= (const Mutex& other)
{
    Mutex tmp(other); // re-use copy-constructor
    std::memcpy(this, &tmp, sizeof(tmp));
    return *this;
}

Mutex::~Mutex()
{
    // Free resources
    unlock();
    pthread_mutex_destroy(lock_);
}

int Mutex::lock()
{
    int retVal=0;
    retVal = pthread_mutex_lock(lock_);
    if(retVal != 0) {   // On error
        locked_ = false;
        throw lock_pthreadMutexLockError_Exception(retVal);
    } else {
        locked_ = true;
    }
    return retVal;
}

int Mutex::unlock()
{
    int retVal=0;
    if(locked_) {
        retVal = pthread_mutex_unlock(lock_);
        if(retVal != 0) {   // On error
            throw unlock_pthreadMutexUnlock_Exception(retVal);
        } else {
            locked_ = false;
        }
    }
    return retVal;
}

/////////////////////////// EXCEPTIONS /////////////////////////////////////////

std::runtime_error Mutex::Mutex_pthreadMutexInitError_Exception(int errorCode)
{
    std::ostringstream oss;
    oss << "Error while creating the Mutex.";
    oss << System::getErrorStr(errorCode);
    return std::runtime_error(oss.str());
}

std::runtime_error Mutex::lock_pthreadMutexLockError_Exception(int errorCode)
{
    std::ostringstream oss;
    oss << "Error while Locking the Mutex. ";
    oss << System::getErrorStr(errorCode);
    return std::runtime_error(oss.str());
}

std::runtime_error Mutex::unlock_pthreadMutexUnlock_Exception(int errorCode)
{
    std::ostringstream oss;
    oss << "Error while Unlocking the Mutex.";
    oss << System::getErrorStr(errorCode);
    return std::runtime_error(oss.str());
}

MutexLocker::MutexLocker() :
    mutex_(NULL)
{
}

MutexLocker::MutexLocker(Mutex *mutex)
{
    mutex_ = mutex;
    relock();
}

MutexLocker::~MutexLocker()
{
    unlock();
}

Mutex *MutexLocker::getMutex()
{
    return mutex_;
}

int MutexLocker::relock()
{
    return mutex_->lock();
}

int MutexLocker::unlock()
{
    return mutex_->unlock();
}

} // namespace core
} // namespace lcg
