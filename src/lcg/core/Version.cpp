// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file Version.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.6.1
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the LCG Core Cpp Library.
//
//                          License Agreement
//                     For the LCG Core Cpp Library
//
//    LCG Core Cpp Library is a lightweight open source library used to wrap
//    pthreads, Berkeley sockets, and other C libraries and functionalities,
//    into C++ objectes, over Linux, Mac OS and Windows.
//
//    LCG Core Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    LCG Core Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include "lcg/core/Version.h"

#include <iostream>

namespace lcg{
namespace core{

Version::Version(const int majorVersion, const int minorVersion, const int patch, const int commit) :
    majorVersion(majorVersion), minorVersion(minorVersion),
    patch(patch), commit(commit)
{
}

void Version::print() const
{
    std::cout << *this;
}

bool Version::isEqual(const Version& other) const
{
    return *this == other;
}

///////////// Operators ///////////////////////

bool Version::operator ==(const Version& other) const
{
    if( (majorVersion == other.majorVersion) && (minorVersion == other.minorVersion) ) {
        return true;
    } else {
        return false;
    }
}

bool Version::operator !=(const Version& other) const
{
    return !(*this==other);
}

bool Version::operator <(const Version& other) const
{
    if(majorVersion < other.majorVersion) {
        return true;
    } else if( (majorVersion == other.majorVersion) && (minorVersion < other.minorVersion) ) {
        return true;
    } else {
        return false;
    }
}

bool Version::operator <=(const Version& other) const
{
    if( (*this < other) || (*this == other) ) {
        return true;
    } else {
        return false;
    }
}

bool Version::operator >(const Version& other) const
{
    return !(*this < other);
}

bool Version::operator >=(const Version& other) const
{
    if( (*this > other) || (*this == other) ) {
        return true;
    } else {
        return false;
    }
}

std::ostream& operator<<(std::ostream& os, const Version& version)
{
    os << &version;
    return os;
}

std::ostream& operator<<(std::ostream& os, const Version* version)
{
    if(version==NULL) {
        throw Version::throwPrintNullException();
    }

    os << version->majorVersion << "." << version->minorVersion << "." <<
          version->patch << "." << version->commit;

    return os;
}

std::runtime_error Version::throwPrintNullException()
{
    return std::runtime_error("Error: Attempting to print null pointer to Version object.");
}

}   // namespace core
}   // namespace lcg
