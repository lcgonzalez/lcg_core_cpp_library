// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file System.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.5.1
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the LCG Core Cpp Library.
//
//                          License Agreement
//                     For the LCG Core Cpp Library
//
//    LCG Core Cpp Library is a lightweight open source library used to wrap
//    pthreads, Berkeley sockets, and other C libraries and functionalities,
//    into C++ objectes, over Linux, Mac OS and Windows.
//
//    LCG Core Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    LCG Core Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include "lcg/core/System.h"

#include <cstring>  // std::strerror
#include <sstream>  // std::ostringstream
#include <stdio.h>  // defines FILENAME_MAX

#ifdef __WINDOWS
    #include <stdlib.h> // _doserrno
    #include <direct.h> // _getcwd
#endif  // __WINDOWS
#ifdef __LINUX
    #include <cerrno>   // errno
    #include <unistd.h> // getcwd
    #include <limits.h>
    #include <stdlib.h> //realpath
#endif  // __LINUX

namespace lcg{
namespace core{

System::System()
{
}

int System::getLastErrorCode(WinErrorType windowsErrorType)
{
    int errorCode = 0;
    switch( windowsErrorType ) {
    case WSAERROR:
        #ifdef __WINDOWS
        errorCode = WSAGetLastError();
        break;
        #endif // __WINDOWS
    case _DOSERRNO:
        #ifdef __WINDOWS
        errorCode = _doserrno;
        break;
        #endif // __WINDOWS
    case ERRNO:
        errorCode = errno;
        break;
    default:
        throw getLastErrorCode_unknownWinErrorType_Exception(windowsErrorType);
    }
    return errorCode;
}

std::string System::getErrorStr(int errorCode)
{
    std::ostringstream oss;
    oss << "Error code # ";
    #ifdef __WINDOWS
    // Note: std::strerror and errno can also be used for Windows, but for file
    // handling on Windows, _doserrno and FormatMessage gives a better description.
    oss << errorCode << " [" << getWindowsErrorString(errorCode) << "]";
    #endif // __WINDOWS
    #ifdef __LINUX
    oss << errorCode << " [" << std::strerror(errorCode) << "]";
    #endif // __LINUX
    return oss.str();
}

std::string System::getCurrentWorkingDir()
{
    char cCurrentPath[FILENAME_MAX];
    if(!getcwd(cCurrentPath, sizeof(cCurrentPath))) {
        // Error
        int errorCode=0;
        #ifdef __WINDOWS
        errorCode = GetLastError();
        #endif // __WINDOWS
        #ifdef __LINUX
        errorCode = errno;
        #endif // __LINUX
        throw System::getCurrentWorkingDir_getcwdError_Exception(errorCode);
    }
    return std::string(cCurrentPath);
}

std::string System::getExeDir()
{
    char* cCurrentPath = NULL;
    int errorCode=0;
    bool isError = false;
    std::string exePath;
    #ifdef __WINDOWS
    cCurrentPath = new char[MAX_PATH];
    int retValue=0;
    // NULL for path of the executable file of the current process
    retValue = GetModuleFileName(NULL, cCurrentPath, MAX_PATH);
    if( retValue == 0) {
        // Error
        errorCode = GetLastError();
        isError = true;
    }
    std::string tmpPath(cCurrentPath);
    exePath = tmpPath;
    //exePath = tmpPath.substr(0,tmpPath.rfind("\\"));    // Strip executable's name from path
    exePath = exePath + "\\";
    #endif // __WINDOWS
    #ifdef __LINUX
    cCurrentPath = realpath("/proc/self/exe", cCurrentPath);
    if( cCurrentPath == NULL) {
        // Error
        errorCode = errno;
        isError = true;
    }
    std::string tmpPath(cCurrentPath);
    exePath = tmpPath.substr(0,tmpPath.rfind("/")); // Strip executable's name from path
    exePath = exePath + "/";
    #endif // __LINUX

    delete[] cCurrentPath;
    if(isError){
        throw System::getExeDir_getPathError_Exception(errorCode);
    }
    return exePath;
}

#ifdef __WINDOWS
std::string System::getWindowsErrorString(int errorCode)
{
    LPVOID  errorStr = NULL;
    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER |
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        errorCode,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPTSTR)&errorStr,
        0, NULL);
    std::string returnStr(static_cast<const char *>(errorStr));
    LocalFree(errorStr);
    return returnStr;
}
#endif // __WINDOWS

/////////////////////////// EXCEPTIONS /////////////////////////////////////////

std::runtime_error System::getCurrentWorkingDir_getcwdError_Exception(int errorCode)
{
    std::ostringstream oss;
    oss << "Error while getting the current working directory. " <<
           System::getErrorStr(errorCode);
    return std::runtime_error(oss.str());
}

std::runtime_error System::getExeDir_getPathError_Exception(int errorCode)
{
    std::ostringstream oss;
    oss << "Error while getting the directory that holds the current executable program file" <<
           System::getErrorStr(errorCode);
    return std::runtime_error(oss.str());
}

std::invalid_argument System::getLastErrorCode_unknownWinErrorType_Exception(int windowsErrorType)
{
    std::ostringstream oss;
    oss << "Error while getting the last error code. The value of " << windowsErrorType << " " <<
           "is unknown. Please use the enumerator \"WinErrorType\" to define the type of " <<
           "Windows error that is requested.";
    return std::invalid_argument(oss.str());
}

} // namespace core
} // namespace lcg
