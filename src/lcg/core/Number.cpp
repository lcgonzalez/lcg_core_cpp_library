// Copyright (C) 2015, 2016 Luis Carlos Gonzalez Garcia. All rights reserved.
/**
//  @file Number.cpp
//  @author Luis Gonzalez <luis@lcgonzalez.com>
//  @version 0.5.1
//  @section LICENSE
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//    This file is part of the LCG Core Cpp Library.
//
//                          License Agreement
//                     For the LCG Core Cpp Library
//
//    LCG Core Cpp Library is a lightweight open source library used to wrap
//    pthreads, Berkeley sockets, and other C libraries and functionalities,
//    into C++ objectes, over Linux, Mac OS and Windows.
//
//    LCG Core Cpp Library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later version.
//
//    LCG Core Cpp Library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, see <http://www.gnu.org/licenses/>,
//    or write to the Free Software Foundation, Inc., 51 Franklin Street,
//    Fifth Floor, Boston, MA  02110-1301  USA.
//*/

#include "lcg/core/Number.h"

#include <sstream>  // std::ostringstream
#include <limits>   // std::numeric_limits

namespace lcg{
namespace core{

std::string Number::intToString (int number){
    std::ostringstream oss;
    oss << number;
    return oss.str();
}

// Specializations definitions
template<>
const int numeric_limits<bool>::max_digits10 = 0;
template<>
const int numeric_limits<char>::max_digits10 = 0;
template<>
const int numeric_limits<signed char>::max_digits10 = 0;
template<>
const int numeric_limits<unsigned char>::max_digits10 = 0;
template<>
const int numeric_limits<wchar_t>::max_digits10 = 0;
template<>
const int numeric_limits<short>::max_digits10 = 0;
template<>
const int numeric_limits<unsigned short>::max_digits10 = 0;
template<>
const int numeric_limits<int>::max_digits10 = 0;
template<>
const int numeric_limits<unsigned int>::max_digits10 = 0;
template<>
const int numeric_limits<long>::max_digits10 = 0;
template<>
const int numeric_limits<unsigned long>::max_digits10 = 0;
template<>
const int numeric_limits<long long>::max_digits10 = 0;
template<>
const int numeric_limits<unsigned long long>::max_digits10 = 0;
template<>
const int numeric_limits<float>::max_digits10 = 2 + std::numeric_limits<float>::digits * 3010/10000;
template<>
const int numeric_limits<double>::max_digits10 = 2 + std::numeric_limits<double>::digits * 3010/10000;
template<>
const int numeric_limits<long double>::max_digits10 = 2 + std::numeric_limits<long double>::digits * 3010/10000;

} // namespace core
} // namespace lcg
